import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

import * as $ from 'jquery'
import * as Highcharts from 'highcharts'
import More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
import threeD from 'highcharts/highcharts-3d';
threeD(Highcharts);
import Exporting from 'highcharts/modules/exporting';
Exporting(Highcharts);

import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { concatMap, tap, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-line-chart',
  // template: `
  // <button class="button button1" (click)="onWeekend()">Weekend!</button>
  // <button class="button button2" (click)="onWeekday()">Weekday!</button>
  // {{clickMessage}}`,
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
  
})
export class LineChartComponent implements OnInit {
  highcharts = Highcharts;

  hourChange$ = new Subject<number>()
  jsons = [
    {
      viewValue: '14.03.2020',
      value: 14
    }, {
      viewValue: '15.03.2020',
      value: 15
    },
    {
      viewValue: '16.03.2020',
      value: 16
    },
    {
      viewValue: '17.03.2020',
      value: 17
    }, {
      viewValue: '18.03.2020',
      value: 18
    }
  ];
  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.init3DbarChartWeekday();
    this.hourChange$.pipe(
      startWith(14),
      concatMap(v => this.httpClient.post<any>('http://localhost:3002/test/maintain',
        {
          start: `2020-03-${v}T00:00:00+00:00`,
          end: `2020-03-${v + 1}T00:00:00+00:00`
        })))
      .subscribe((d: any) => {
        d3.select('#line > svg').remove()
        this.initLineChart(d);
      });
  }

  onValueChange(event) {
    this.hourChange$.next(event.value)
  }

  onWeekday() {
    this.init3DbarChartWeekday();
  }

  onWeekend() {
    this.init3DbarChartWeekend();
  }

  private initLineChart(dataset) {
    const margin = { top: 20, right: 20, bottom: 30, left: 40 }
    const width = 1200;
    const height = 750;

    const n = 24;

    const xScale = d3.scaleLinear()
      .domain([0, n - 1])
      .range([0, width]);

    const yScale = d3.scaleLinear()
      .domain([0, 25])
      .range([height, 0]);

    const line = d3.line()
      .x((d, i) => xScale(i))
      .y((d) => yScale(d.byt))
      .curve(d3.curveMonotoneX)

    const svg = d3.select('#line').append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    const xAxis = d3.axisBottom(xScale)
      .ticks(24);
    svg.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis)

    svg.append("text")
      .attr("transform",
        "translate(" + (width / 2) + " ," +
        (height + margin.top + 5) + ")")
      .style("text-anchor", "middle")
      .text("Hour");

    svg.append('g')
      .attr('class', 'y axis')
      .call(d3.axisLeft(yScale));

    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("Traffic [GB]");

    svg.append('path')
      .datum(dataset)
      .attr('fill', 'none')
      .attr('stroke', '#ffab00')
      .attr('stroke-width', '3')
      .attr('d', line);

      svg.selectAll('.dot')
      .data(dataset)
      .enter().append('circle')
      .attr('fill', '#ffab00')
      .attr('cx', (d, i) => xScale(i))
      .attr('cy', (d) => yScale(d.byt))
      .attr('r', 5);

      let newDataset = [];
      let id = [];
      let count = 0;
      for(let i = 0; i < dataset.length; i++)
      {
        if(dataset[i].byt < 0.1)
        {
          newDataset[count] = dataset[i];
          id[count] = i;
          count++;
        }
      }

      let idCount = 0;
      svg.selectAll('.dot')
      .data(newDataset)
      .enter().append('circle')
      .attr('fill', '#ff0000')
      .attr('cx', function(d) { idCount++; return xScale(id[idCount-1])})
      .attr('cy', (d) => yScale(d.byt))
      .attr('r', 5);

      svg.append("circle").attr("cx", 1100).attr("cy", 50).attr("r", 6).style("fill", "#FF0000")
      svg.append("text").attr("x", 1120).attr("y", 50).text("missing data").style("font-size", "15px").attr("alignment-baseline", "middle")
  }

  private init3DbarChartWeekday() {
    const rand = function (from, to) {
      return Math.round(from + Math.random() * (to - from));
    };

    const opti: any = {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 20,
          beta: 30,
          depth: 400, // Set deph
          viewDistance: 3,
          frame: {
            bottom: {
              size: 1,
              color: 'rgba(0,0,0,0.05)'
            }
          }
        }
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      yAxis: {
        min: 0,
        max: 10
      },
      xAxis: {
        min: 0,
        max: 24,
        gridLineWidth: 1,
      },
      zAxis: {
        min: 0,
        max: 4,
        categories: ['16.03.2020', '17.03.2020', '18.03.2020', '19.03.2020', '20.03.2020', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12'],
        labels: {
          y: 5,
          rotation: 18
        }
      },
      plotOptions: {
        series: {
          groupZPadding: 10,
          depth: 70,
          groupPadding: 0,
          grouping: false,
        }
      },
      series: [{
        name: '16.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }, {
        name: '17.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }, {
        name: '18.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }, {
        name: '19.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      },
      {
        name: '20.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }]
    };

    const chart = this.highcharts.chart('container', opti);

    // Add mouse events for rotation
    $(chart.container).on('mousedown.hc touchstart.hc', function (eStart: any) {
      eStart = chart.pointer.normalize(eStart);

      var posX = eStart.pageX,
        posY = eStart.pageY,
        alpha = chart.options.chart.options3d.alpha,
        beta = chart.options.chart.options3d.beta,
        newAlpha,
        newBeta,
        sensitivity = 5; // lower is more sensitive

      $(document).on({
        'mousemove.hc touchdrag.hc': function (e) {
          // Run beta
          newBeta = beta + (posX - e.pageX) / sensitivity;
          chart.options.chart.options3d.beta = newBeta;

          // Run alpha
          newAlpha = alpha + (e.pageY - posY) / sensitivity;
          chart.options.chart.options3d.alpha = newAlpha;

          chart.redraw(false);
        },
        'mouseup touchend': function () {
          $(document).off('.hc');
        }
      });
    });
  }

  private init3DbarChartWeekend() {
    const rand = function (from, to) {
      return Math.round(from + Math.random() * (to - from));
    };

    const opti: any = {
      chart: {
        type: 'column',
        options3d: {
          enabled: true,
          alpha: 20,
          beta: 30,
          depth: 400, // Set deph
          viewDistance: 3,
          frame: {
            bottom: {
              size: 1,
              color: 'rgba(0,0,0,0.05)'
            }
          }
        }
      },
      title: {
        text: ''
      },
      subtitle: {
        text: ''
      },
      yAxis: {
        min: 0,
        max: 10
      },
      xAxis: {
        min: 0,
        max: 24,
        gridLineWidth: 1,
      },
      zAxis: {
        min: 0,
        max: 1,
        categories: ['14.03.2020', '15.03.2020', '18.03.2020', '19.03.2020', '20.03.2020', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12'],
        labels: {
          y: 5,
          rotation: 18
        }
      },
      plotOptions: {
        series: {
          groupZPadding: 10,
          depth: 160,
          groupPadding: 0,
          grouping: false,
        }
      },
      series: [{
        name: '14.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }, {
        name: '15.03.2020',
        data: [...Array(24)].map((v, i) => ({ x: i, y: rand(0, 10) }))
      }]
    };

    const chart = this.highcharts.chart('container', opti);

    // Add mouse events for rotation
    $(chart.container).on('mousedown.hc touchstart.hc', function (eStart: any) {
      eStart = chart.pointer.normalize(eStart);

      var posX = eStart.pageX,
        posY = eStart.pageY,
        alpha = chart.options.chart.options3d.alpha,
        beta = chart.options.chart.options3d.beta,
        newAlpha,
        newBeta,
        sensitivity = 5; // lower is more sensitive

      $(document).on({
        'mousemove.hc touchdrag.hc': function (e) {
          // Run beta
          newBeta = beta + (posX - e.pageX) / sensitivity;
          chart.options.chart.options3d.beta = newBeta;

          // Run alpha
          newAlpha = alpha + (e.pageY - posY) / sensitivity;
          chart.options.chart.options3d.alpha = newAlpha;

          chart.redraw(false);
        },
        'mouseup touchend': function () {
          $(document).off('.hc');
        }
      });
    });
  }
}
