import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NpnSliderModule } from "npn-slider";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { MapComponent } from './map/map.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LandingComponent } from './landing/landing.component';
import { ScatterPlotComponent } from './scatter-plot/scatterplot.component';
import { ThreeComponent } from './three/three.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TimeSeriesComponent } from './time-series/time-series.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatSelectModule } from '@angular/material/select';
import { AreaChartComponent } from './area-chart/area-chart.component';
import { ClockmapComponent } from './clockmap/clockmap.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { LegendComponent } from './legend/legend.component';

import * as $ from 'jquery';
import { LineClientsComponent } from './line-clients/line-clients.component';
import { SunburstComponent } from './sunburst/sunburst.component';
import { TimezonesComponent } from './timezones/timezones.component'

@NgModule({
  declarations: [
    AppComponent,
    BarChartComponent,
    MapComponent,
    PieChartComponent,
    LandingComponent,
    ScatterPlotComponent,
    ThreeComponent,
    TimeSeriesComponent,
    AreaChartComponent,
    ClockmapComponent,
    LineChartComponent,
    LegendComponent,
    LineClientsComponent,
    SunburstComponent,
    TimezonesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxMapboxGLModule.withConfig({
      accessToken:
        'pk.eyJ1IjoibWVyZXRoIiwiYSI6ImNqbXA3Z2ZoNzB4YzIzeHFjb3VwdXJuN2oifQ.LlSXvjk8lCZDBl6OYq0xdA',
      // accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA'
    }),
    MatSidenavModule,
    NpnSliderModule,
    MatInputModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    HttpClientModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    FlexLayoutModule,
    MatSliderModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon(
      'vd-netflow',
      sanitizer.bypassSecurityTrustResourceUrl(
        'assets/network_check-black-18dp.svg'
      )
    );
    iconRegistry.addSvgIcon(
      'github',
      sanitizer.bypassSecurityTrustResourceUrl('assets/github.svg')
    );
  }
}
