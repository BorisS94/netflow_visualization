import { Component, ViewEncapsulation, OnInit, ElementRef, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import * as d3 from 'd3'
import { BaseChartComponent } from '../base-chart.component';
import { State } from '../state';
import { HttpClient } from '@angular/common/http';
import { map, concatMap, switchMap, tap, take } from 'rxjs/operators';
import { Subject, concat } from 'rxjs';


interface Data {
  protocol: string;
  amount: number;
}

@Component({
  selector: 'app-scatterplot',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './scatterplot.component.html',
  styleUrls: ['./scatterplot.component.scss']
})
export class ScatterPlotComponent extends BaseChartComponent implements OnInit {

  title = 'ScatterPlot';
  @Input() hour$: Subject<number>

  currentHour;

  private margin = { top: 20, right: 20, bottom: 30, left: 50 };
  private width: number;
  private height: number;
  private radius: number;

  private data: Data[]

  public dataBar: any[]
  public dataPie: any[]
  constructor(protected elRef: ElementRef, protected state: State, protected cd: ChangeDetectorRef, private httpClient: HttpClient) {
    super(elRef, state, cd)

  }
  async ngOnInit() {
    super.ngOnInit();

    this.hour$.pipe(tap(v => {
      this.currentHour = v
    }), concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
      {
        start: `2020-02-19T${v < 10 ? '0' + v : v}:00:00+00:00`,
        end: `2020-02-19T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`,
      },
    ))).subscribe(dataBar => {
      this.dataBar = dataBar;
      this.httpClient.post<Data[]>('http://localhost:3002/test/pie-data',
        {
        start: `2020-02-19T${this.currentHour < 10 ? '0' + this.currentHour : this.currentHour}:00:00+00:00`,
        end: `2020-02-19T${this.currentHour + 1 < 10 ? '0' + (this.currentHour + 1) : this.currentHour + 1}:00:00+00:00`,
        }
      ).subscribe(dataPie => {
        this.dataPie = dataPie;
        this.drawDonut();
      })
    })

    /*this.httpClient.post<Data[]>('http://localhost:3002/test/pie-data',
      {
        start: `2020-03-14T08:00:00+00:00`,
        end: `2020-03-14T09:00:00+00:00`
      }
    ).subscribe(dataPie => {
      this.dataPie = dataPie;
      this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
        {
          start: `2020-03-14T08:00:00+00:00`,
          end: `2020-03-14T09:00:00+00:00`
        }
      ).subscribe(dataBar => {
        this.dataBar = dataBar;
        this.drawDonut();
      })
    })*/


  }

  private drawDonut() {

    this.width = 900 - this.margin.left - this.margin.right;
    this.height = 500 - this.margin.top - this.margin.bottom;
    this.radius = Math.min(this.width, this.height) / 2;

    let labelsD = [];
    let packets = [];
    let bytes = [];
    let count = 0;

    for (let i = 0; i < this.dataPie.length; i++) {
      for (let j = 0; j < this.dataBar.length; j++) {
        if (this.dataPie[i]._id == this.dataBar[j]._id && this.dataPie[i].pkt > 50000) {
          bytes[count] = Math.round(this.dataBar[j].byt * 1000000);
          packets[count] = this.dataPie[i].pkt;
          labelsD[count] = this.dataPie[i]._id;
          count++;
        }
      }
    }

    var data = {
      labels: labelsD,
      series: [
        {
          label: 'packets',
          values: packets
        },
        {
          label: 'bytes',
          values: bytes
        }]
    };

    console.log(data);

    /*var data = {
      labels: [
        'resilience', 'maintainability', 'accessibility',
        'uptime', 'functionality', 'impact'
      ],
      series: [
        {
          label: 'packets',
          values: [40000, 80000, 150000, 160000, 230000, 420000]
        },
        {
          label: 'bytes',
          values: [120000, 430000, 220000, 110000, 730000, 250000]
        }]
    };*/

    var chartWidth = 300,
      barHeight = 20,
      groupHeight = barHeight * data.series.length,
      gapBetweenGroups = 10,
      spaceForLabels = 150,
      spaceForLegend = 150;

    // Zip the series data together (first values, second values, etc.)
    var zippedData = [];
    for (var i = 0; i < data.labels.length; i++) {
      for (var j = 0; j < data.series.length; j++) {
        zippedData.push(data.series[j].values[i]);
      }
    }

    // Color scale
    var color = d3.scaleOrdinal(d3.schemeCategory10);
    var chartHeight = barHeight * zippedData.length + gapBetweenGroups * data.labels.length;

    var x = d3.scaleLinear()
      .domain([0, d3.max(zippedData)])
      .range([0, chartWidth]);

    var y = d3.scaleLinear()
      .range([chartHeight + gapBetweenGroups, 0]);

    var yAxis = d3.axisLeft()
      .scale(y)
      .tickFormat('')
      .tickSize(0)

    // Specify the chart area and dimensions
    var chart = d3.select(".aa")
      .attr("width", spaceForLabels + chartWidth + spaceForLegend + 200)
      .attr("height", chartHeight);

    // Create bars
    var bar = chart.selectAll("g")
      .data(zippedData)
      .enter().append("g")
      .attr("transform", function (d, i) {
        return "translate(" + spaceForLabels + "," + (i * barHeight + gapBetweenGroups * (0.5 + Math.floor(i / data.series.length))) + ")";
      });

    // Create rectangles of the correct width
    bar.append("rect")
      .attr("fill", function (d, i) { return color(i % data.series.length); })
      .attr("class", "bar")
      .attr("width", x)
      .attr("height", barHeight - 1);

    // Add text label in bar
    bar.append("text")
      .attr("x", function (d) { return x(d) + 50; })
      .attr("y", barHeight / 2)
      .attr("fill", "red")
      .attr("dy", ".35em")
      .text(function (d) { return d; });


    // Draw labels
    bar.append("text")
      .attr("class", "label")
      .attr("x", function (d) { return - 10; })
      .attr("y", groupHeight / 2)
      .attr("dy", ".35em")
      .text(function (d, i) {
        if (i % data.series.length === 0)
          return data.labels[Math.floor(i / data.series.length)];
        else
          return ""
      });

    chart.append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(" + spaceForLabels + ", " + -gapBetweenGroups / 2 + ")")
      .call(yAxis);

    // Draw legend
    var legendRectSize = 18,
      legendSpacing = 4;

    var legend = chart.selectAll('.legend')
      .data(data.series)
      .enter()
      .append('g')
      .attr('transform', function (d, i) {
        var height = legendRectSize + legendSpacing;
        var offset = -gapBetweenGroups / 2;
        var horz = spaceForLabels + chartWidth + 90 - legendRectSize;
        var vert = i * height - offset;
        return 'translate(' + horz + ',' + vert + ')';
      });

    legend.append('rect')
      .attr('width', legendRectSize)
      .attr('height', legendRectSize)
      .style('fill', function (d, i) { return color(i); })
      .style('stroke', function (d, i) { return color(i); });

    legend.append('text')
      .attr('class', 'legend')
      .attr('x', legendRectSize + legendSpacing)
      .attr('y', legendRectSize - legendSpacing)
      .text(function (d) { return d.label; });
  }
}
