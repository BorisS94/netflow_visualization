import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

@Component({
  selector: 'app-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.scss']
})
export class LegendComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.addColorLegend();
  }

  private addColorLegend() {
    var w = 320, h = 50;

    var key = d3.select("#legend1")
      .append("svg")
      .attr("width", w + 20)
      .attr("height", h);

    var legend = key.append("defs")
      .append("svg:linearGradient")
      .attr("id", "gradient")
      .attr("x1", "0%")
      .attr("y1", "100%")
      .attr("x2", "100%")
      .attr("y2", "100%")
      .attr("spreadMethod", "pad");

    legend.append("stop")
      .attr("offset", "0%")
      .attr("stop-color", "#00ff00")
      .attr("stop-opacity", 1);

    legend.append("stop")
      .attr("offset", "20%")
      .attr("stop-color", "#66ff00")
      .attr("stop-opacity", 1);

    legend.append("stop")
      .attr("offset", "40%")
      .attr("stop-color", "#ccff00")
      .attr("stop-opacity", 1);

    legend.append("stop")
      .attr("offset", "60%")
      .attr("stop-color", "#ffcc00")
      .attr("stop-opacity", 1);

    legend.append("stop")
      .attr("offset", "80%")
      .attr("stop-color", "#ff6600")
      .attr("stop-opacity", 1);

    legend.append("stop")
      .attr("offset", "100%")
      .attr("stop-color", "#ff0000")
      .attr("stop-opacity", 1);

    key.append("rect")
      .attr("width", w)
      .attr("height", h - 30)
      .style("fill", "url(#gradient)")
      .attr("transform", "translate(0,30)");

    // create svg element
    var svg = d3.select("#legend1")
      .append("svg")
      .attr("width", 1000)
      .attr("height", 300)

    var y = d3.scaleLinear()
      .range([320, 0])
      .domain([100, 0]);

    svg.append("g")
      .attr("transform", "translate(0,0)")
      .call(d3.axisBottom(y))
      .selectAll("text")
      .attr("transform", "translate(11,10)rotate(0)")
      .style("text-anchor", "end")
      .style("font-size", 15)
      .style("fill", "#69a3b2")
  }

}
