import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3'

@Component({
  selector: 'app-time-series',
  templateUrl: './time-series.component.html',
  styleUrls: ['./time-series.component.scss']
})
export class TimeSeriesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.timeSeries();
  }

  private timeSeries() {
      var oneNorthData = [
        {
          startHour: 6,
          endHour: 10
        },
        {
          startHour: 14,
          endHour: 17
        },
        {
          startHour: 7,
          endHour: 11
        },
        {
          startHour: 0,
          endHour: 1
        },
        {
          startHour: 8,
          endHour: 9
        }
      ]
  
      var margin = { top: 10, right: 10, bottom: 10, left: 30 },
        elementWidth = parseInt(d3.select(".oneSouth").style("width")),
        elementHeight = parseInt(d3.select(".oneSouth").style("height")),
        width = elementWidth - margin.left - margin.right,
        height = elementHeight - margin.top - margin.bottom;
  
      //linear 24 hour scale
      var y = d3.scaleLinear()
        .domain([24, 0])
        .range([height, 0]);
  
      //vertical axis
      var yAxis = d3.axisRight(y)
        .ticks(24)
        .scale(y)
  
      var tickArray = y.ticks(24),
        tickDistance = y(tickArray[tickArray.length - 1]) - y(tickArray[tickArray.length - 2]);
  
      //create the svg
      var svg = d3.select(".oneSouth").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
  
      //draw the axis
      svg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(yAxis)
        .selectAll("text")
        .attr("transform", "translate(-25,0)")
  
      svg.selectAll(".bar")
        .data(oneNorthData)
        .enter()
        .append("rect")
        .attr("class", "bar")
        .attr("x", margin.left + 20)
        .attr("width", Math.abs(tickDistance))
        .attr("y", function (d) {
          return y(d.startHour) + margin.top;
        })
        .attr("height", function (d) {
          var offset = d.endHour - d.startHour;
          return offset * Math.abs(tickDistance);
        })
        .style('fill', 'steelblue')
        .style('opacity', '.5');
    }
}
