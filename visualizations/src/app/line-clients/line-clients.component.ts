import { Component, ViewEncapsulation, OnInit, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, Input } from '@angular/core';
import * as d3 from 'd3';
import { ScaleBand, ScaleLinear } from 'd3';
import { BaseChartComponent } from '../base-chart.component';
import { State } from '../state';
import { HttpClient } from '@angular/common/http';
import { filter, take, concatMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

interface Data {
  _id: string;
  byt: number;
}

@Component({
  selector: 'app-line-clients',
  templateUrl: './line-clients.component.html',
  styleUrls: ['./line-clients.component.scss']
})
export class LineClientsComponent extends BaseChartComponent implements OnInit {
  @Input() hour$: Subject<number>

  constructor(protected elRef: ElementRef, protected state: State, protected cd: ChangeDetectorRef, private httpClient: HttpClient) {
    super(elRef, state, cd)
  }

  async ngOnInit() {
    super.ngOnInit();

    this.hour$.pipe(concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
      {
        start: `2020-03-14T${v < 10 ? '0' + v : v}:00:00+00:00`,
        end: `2020-03-14T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`
      }
    ))).subscribe(data => {
      this.initLineChart();
    })
  }

  private initLineChart() {
    var margin = { top: 20, right: 20, bottom: 30, left: 40 }
      , width = 400
      , height = 250;

    var n = 24;

    var xScale = d3.scaleLinear()
      .domain([0, n - 1])
      .range([0, width]);

    var yScale = d3.scaleLinear()
      .domain([0, 1])
      .range([height, 0]);

    var line = d3.line()
      .x(function (d, i) { return xScale(i); })
      .y(function (d) { return yScale(d.y); })
      .curve(d3.curveMonotoneX)

    var dataset = [{ y: 0.12 }, { y: 0.22 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 },
    { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }, { y: 0.12 }]
    console.log(dataset)

    var svg = d3.select("#cli").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale));

    svg.append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(yScale));

    svg.append("path")
      .datum(dataset)
      .attr('fill', 'none')
      .attr('stroke', '#ffab00')
      .attr('stroke-width', '3')
      .attr("d", line);

    svg.selectAll(".dot")
      .data(dataset)
      .enter().append("circle")
      .attr("fill", "#ffab00")
      .attr("cx", function (d, i) { return xScale(i) })
      .attr("cy", function (d) { return yScale(d.y) })
      .attr("r", 5);
  }

}
