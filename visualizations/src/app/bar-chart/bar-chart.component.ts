import { Component, ViewEncapsulation, OnInit, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, Input } from '@angular/core';
import * as d3 from 'd3';
import { ScaleBand, ScaleLinear } from 'd3';
import { BaseChartComponent } from '../base-chart.component';
import { State } from '../state';
import { HttpClient } from '@angular/common/http';
import { filter, take, concatMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

interface Data {
    _id: string;
    byt: number;
}

@Component({
    selector: 'app-bar-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chart.component.html',
    styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent extends BaseChartComponent implements OnInit {

    title = 'Bar Chart';
    @Input() hour$: Subject<number>


    private width: number;
    private height: number;
    private margin = { top: 20, right: 20, bottom: 30, left: 40 };

    private x: ScaleBand<any>;
    private y: ScaleLinear<any, any>;
    // private svg: Selection<SVGSVGElement, any, any, any>;
    private data: Data[];

    constructor(protected elRef: ElementRef, protected state: State, protected cd: ChangeDetectorRef, private httpClient: HttpClient) {
        super(elRef, state, cd)
    }

    async ngOnInit() {
        super.ngOnInit()
        this.initSvg();

        /*this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
            {
                start: '2020-02-19T13:00:00+00:00',
                end: '2020-02-19T14:00:00+00:00'
            }
        ).pipe(take(1)).subscribe(data => {
            console.log('bar', data)
            this.initSvg()
            const less = data.filter(({ byt }) => byt < 0.01)
                .reduce((prev, { byt }) => ({ ...prev, byt: prev.byt + byt }), { _id: 'REST', byt: 0 })
            const more = data.filter(({ byt }) => byt >= 0.01);
            this.data = [...more, less];
            this.initAxis(this.data);
            this.drawAxis('Traffic [GB]');
            this.drawBars(this.data);
        })*/

        this.hour$.pipe(concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
            {
                start: `2020-02-19T${v < 10 ? '0' + v : v}:00:00+00:00`,
                end: `2020-02-19T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`
            }
        ))).subscribe(data => {
            console.log('bar', data)
            this.initSvg()
            const less = data.filter(({ byt }) => byt < 0.01)
                .reduce((prev, { byt }) => ({ ...prev, byt: prev.byt + byt }), { _id: 'REST', byt: 0 })
            const more = data.filter(({ byt }) => byt >= 0.01);
            this.data = [...more, less];
            this.initAxis(this.data);
            this.drawAxis('Traffic [GB]');
            this.drawBars(this.data);
        })
    }

    private initSvg() {

        this.width = +this.svg.attr('width') - this.margin.left - this.margin.right;
        this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;
        this.g = this.svg.append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
    }

    private initAxis(data: Data[]) {
        this.x = d3.scaleBand().rangeRound([0, this.width]).padding(0.1);
        this.y = d3.scaleLinear().rangeRound([this.height, 0]);
        this.x.domain(this.data.map((d) => d._id));
        this.y.domain([0, d3.max(this.data, (d) => d.byt)]);
    }

    private drawAxis(text) {
        this.g.append('g')
            .attr('class', 'axis axis--x')
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(d3.axisBottom(this.x));
        this.g.append('g')
            .attr('class', 'axis axis--y')
            .call(d3.axisLeft(this.y))
            .append('text')
            .attr('class', 'axis-title')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('dy', '0.71em')
            .attr('text-anchor', 'end')
            .text(text);
    }

    private drawBars(data: Data[]) {
        this.g.selectAll('.bar')
            .data(this.data)
            .enter()
            .append('rect')
            .attr('class', 'bar')
            .attr('x', d => this.x(d._id))
            .attr('y', d => this.y(d.byt))
            .attr('width', this.x.bandwidth())
            .attr('height', d => this.height - this.y(d.byt))
    }

}