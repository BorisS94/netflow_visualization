import { Component, OnInit } from '@angular/core';
import { Layer } from 'mapbox-gl';
import { State } from '../state';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  earthquakes: object;
  clusterLayers: Layer[];

  constructor(private state: State, private httpClient: HttpClient) {
    this.state.toggleProgressBar()
    this.httpClient.get('http://localhost:3002/test/providers').subscribe(geo => {
      this.earthquakes = geo;
      const layersData: [number, string][] = [
        [0, 'green'],
        [20, 'orange'],
        [200, 'red']
      ];
      this.clusterLayers = layersData.map((data, index) => ({
        id: `cluster-${index}`,
        paint: {
          'circle-color': data[1],
          'circle-radius': 70,
          'circle-blur': 1
        },
        filter:
          index === layersData.length - 1
            ? ['>=', 'point_count', data[0]]
            : ['all', ['>=', 'point_count', data[0]], ['<', 'point_count', layersData[index + 1][0]]]
      }));
      this.state.toggleProgressBar()
    })
  }

  ngOnInit() {
    // this.earthquakes = await import('./earthquakes.geo.json');
    

  }

  onMapLoaded() {
    console.log('map loaded')
    
  }

}
