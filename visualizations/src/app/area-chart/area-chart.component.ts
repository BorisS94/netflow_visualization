import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { HttpClient } from '@angular/common/http';
import { Subject, zip, of } from 'rxjs';
import { startWith, concatMap, map, catchError, filter, retry } from 'rxjs/operators';

@Component({
  selector: 'app-area-chart',
  templateUrl: './area-chart.component.html',
  styleUrls: ['./area-chart.component.scss'],
})
export class AreaChartComponent implements OnInit {
  data = [];

  hourChange$ = new Subject<number>()
  jsons = [
    {
      viewValue: '14.03.2020',
      value: 14
    }, {
      viewValue: '15.03.2020',
      value: 15
    },
    {
      viewValue: '16.03.2020',
      value: 16
    },
    {
      viewValue: '17.03.2020',
      value: 17
    }, {
      viewValue: '18.03.2020',
      value: 18
    }
  ];

  constructor(private httpClient: HttpClient) { }

  onValueChange(event) {
    this.hourChange$.next(event.value)
  }

  ngOnInit(): void {

    const udpAttacks$ = (day: string) => this.httpClient.get(`http://localhost:3002/test/udp-flood/${day}`);
    const synAttacks$ = (day: string) => this.httpClient.get(`http://localhost:3002/test/syn-flood/${day}`);
    const rstAttacks$ = (day: string) => this.httpClient.get(`http://localhost:3002/test/rst/${day}`);

    this.hourChange$.pipe(
      startWith(14),
      map(v => `2020-03-${v}T00:00:00+00:00`),
      concatMap(v => zip(udpAttacks$(v), synAttacks$(v), rstAttacks$(v))),
      map(([udp, syn, rst]) => Array(24).fill({}).map((_, i) => ({ year: i, aData: udp[i] ?? 0, bData: syn[i] ?? 0, cData: rst[i] ?? 0 }))),
      retry(),
    ).subscribe(data => {
      this.data = data;
      d3.select('#area > svg').remove()
      this.chart();
    });
  }

  chart() {
    const color = ['rgba(127, 191, 63, 1.0)', 'rgba(63, 127, 191, 1.0)', 'rgba(191, 63, 63, 1.0)'];

    // Create SVG and padding for the chart
    const svg = d3
      .select('#area')
      .append('svg')
      .attr('height', 700)
      .attr('width', 1200);

    const strokeWidth = 1.5;
    const margin = { top: 0, bottom: 40, left: 65, right: 20 };
    const chart = svg
      .append('g')
      .attr('transform', `translate(${margin.left},0)`);

    const width =
      +svg.attr('width') - margin.left - margin.right - strokeWidth * 2;
    const height = +svg.attr('height') - margin.top - margin.bottom;
    const grp = chart
      .append('g')
      .attr(
        'transform',
        `translate(-${margin.left - strokeWidth},-${margin.top})`
      );

    // Create stack
    const stack = d3.stack().keys(['aData', 'bData', 'cData']);
    const stackedValues = stack(this.data);
    const stackedData = [];
    // Copy the stack offsets back into the data.
    stackedValues.forEach((layer, index) => {
      const currentStack = [];
      layer.forEach((d, i) => {
        currentStack.push({
          values: d,
          year: this.data[i].year,
        });
      });
      stackedData.push(currentStack);
    });

    // Create scales
    const yScale = d3
      .scaleLinear()
      .range([height, 0])
      .domain([
        0,
        d3.max(stackedValues[stackedValues.length - 1], (dp) => dp[1]),
      ]);
    const xScale = d3
      .scaleLinear()
      .range([0, width])
      .domain(d3.extent(this.data, (dataPoint) => dataPoint.year));

    const area = d3
      .area()
      .x((dataPoint) => xScale(dataPoint.year))
      .y0((dataPoint) => yScale(dataPoint.values[0]))
      .y1((dataPoint) => yScale(dataPoint.values[1]));

    const series = grp
      .selectAll('.series')
      .data(stackedData)
      .enter()
      .append('g')
      .attr('class', 'series');

    series
      .append('path')
      .attr('transform', `translate(${margin.left},0)`)
      .style('fill', (d, i) => color[i])
      .style('opacity', 0.5)
      .attr('stroke', 'steelblue')
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr('stroke-width', strokeWidth)
      .attr('d', (d) => area(d));

    // Add the X Axis
    chart
      .append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(xScale).ticks(this.data.length));

    svg.append("text")
      .attr("transform",
        "translate(" + (width / 2) + " ," +
        (height + margin.top + 35) + ")")
      .style("text-anchor", "middle")
      .text("Hour");

    // Add the Y Axis
    chart
      .append('g')
      .attr('transform', `translate(0, 0)`)
      .call(d3.axisLeft(yScale));

    svg.append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 0 - margin.left + 62)
      .attr("x", 0 - (height / 2))
      .attr("dy", "1em")
      .style("text-anchor", "middle")
      .text("Attacks");

    svg.append("circle").attr("cx", 1100).attr("cy", 120).attr("r", 6).style("fill", "#7FBF3F")
    svg.append("circle").attr("cx", 1100).attr("cy", 150).attr("r", 6).style("fill", "#3F7FBF")
    svg.append("circle").attr("cx", 1100).attr("cy", 180).attr("r", 6).style("fill", "#BF3F3F")
    svg.append("text").attr("x", 1120).attr("y", 120).text("UDP flood").style("font-size", "15px").attr("alignment-baseline", "middle")
    svg.append("text").attr("x", 1120).attr("y", 150).text("SYN flood").style("font-size", "15px").attr("alignment-baseline", "middle")
    svg.append("text").attr("x", 1120).attr("y", 180).text("RST").style("font-size", "15px").attr("alignment-baseline", "middle")
  }
}
