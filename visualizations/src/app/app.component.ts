import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ROUTES } from './app-routing.module';
import { State } from './state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  routes = ROUTES.filter(({path}) => !!path);
  inProgress$: Observable<boolean>
  info$
  constructor(private state: State) {
    this.inProgress$ = this.state.select('inProgress');
    this.info$ = this.state.select('info')
  }
}
