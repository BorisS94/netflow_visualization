import { ElementRef, OnInit, ChangeDetectorRef } from '@angular/core';
import { Selection, select} from 'd3'
import { State } from './state';

export abstract class BaseChartComponent implements OnInit {
  protected hostElement: HTMLElement;
  protected svg: Selection<SVGSVGElement, any, any, any>;
  protected g: Selection<SVGGElement, any, any, any>;
  
  constructor(protected elRef: ElementRef, protected state: State, protected cd: ChangeDetectorRef) {
    this.hostElement = elRef.nativeElement;
  }

  ngOnInit() {
    // this.state.toggleProgressBar()
    this.svg = select(this.getSvgFromHost())
  }

  private getSvgFromHost() {
    return this.hostElement.getElementsByTagName('svg')[0]
  }

}