import { BehaviorSubject, Observable } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators'
import { Injectable } from '@angular/core';

interface AppState {
  inProgress: boolean;
  data: any[]
  info: any
}

const INIT_STATE: AppState = {
  inProgress: false,
  data: [],
  info: {}
}

@Injectable({
  providedIn: 'root'
})
export class State {
  private state = new BehaviorSubject<AppState>(INIT_STATE);
  private stateGetter = this.state.asObservable();

  getData(): AppState {
    return JSON.parse(JSON.stringify(this.state.value));
  }

  patchData(data: any): void {
    const state = this.state.value;
    console.log('patching', data, state)
    const newState = {
        ...state,
        ...data
      }
    this.state.next(newState);
  }

  reset(): void {
    this.state.next(INIT_STATE);
  }

  select(...args: string[]): Observable<any> {
    const get = this.stateGetter.pipe(
      map((state: AppState) => {
        return args.reduce((resolve, curr) => {
          console.log('selecting ',curr, state, state[curr])
          return { ...resolve, [curr]: state[curr] ?? undefined };
        }, {});
      }),
      distinctUntilChanged()
    );
    return args.length === 1 ? get.pipe(map(res => res[args[0]])) : get
  }

  toggleProgressBar() {
    const {inProgress} = this.state.value;
    console.log('toggling',inProgress)
    this.patchData({inProgress: !inProgress})
  }
}