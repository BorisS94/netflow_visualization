import { Component, ViewEncapsulation, OnInit, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, Input, SimpleChanges, OnChanges } from '@angular/core';
import * as d3 from 'd3'
import { State } from '../state';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { take, concatMap } from 'rxjs/operators';


@Component({
  selector: 'app-sunburst',
  templateUrl: './sunburst.component.html',
  styleUrls: ['./sunburst.component.scss']
})
export class SunburstComponent implements OnInit {
  private data: any;
  private dataNew: any;

  private inputValue = 40;

  hourChange$ = new Subject<number>()
  jsons = [
    {
      viewValue: '09:00 - 10:00',
      value: 9
    }, {
      viewValue: '10:00 - 11:00',
      value: 10
    },
    {
      viewValue: '11:00 - 12:00',
      value: 11
    },
    {
      viewValue: '12:00 - 13:00',
      value: 12
    },
    {
      viewValue: '13:00 - 14:00',
      value: 13
    },
    {
      viewValue: '14:00 - 15:00',
      value: 14
    },
    {
      viewValue: '15:00 - 16:00',
      value: 15
    },
    {
      viewValue: '16:00 - 17:00',
      value: 16
    }
  ]

  onValueChange(event) {
    this.hourChange$.next(event.value)
  }

  onKey(event) { this.inputValue = event.target.value; }

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.httpClient.post('http://localhost:3002/test/providers',
      {
        start: 13,
        amount: this.inputValue * 1000000
      }
    ).subscribe((data: any[]) => {
      console.log("providers", data)
      d3.select('#clock > svg').remove()
      this.data = data;
      this.clockmap();
    })

    this.hourChange$.pipe(concatMap(v => this.httpClient.post('http://localhost:3002/test/providers',
      {
        start: v,
        amount: this.inputValue * 1000000
      }
    ))).subscribe((data: any[]) => {
      console.log("providers", data)
      d3.select('#clock > svg').remove()
      this.data = data;
      this.clockmap();
    })
    this.clockmap();
  }

  private clockmap() {
    /*var data = {
      "name": "test",
      "children": [
        {
          "name": "world1",
          "children": [
            {
              "name": "SK2",
              "children": [
                {
                  "name": "111.111.111.111", "size": 388
                },
                {
                  "name": "222.222.222.222", "size": 377
                },
                {
                  "name": "333.333.333.333", "size": 1110
                },
                {
                  "name": "444.444.444.444", "size": 100
                },
                {
                  "name": "888.888.888.888", "size": 910
                },
                {
                  "name": "888.888.888.888", "size": 90
                },
                {
                  "name": "888.888.888.888", "size": 890
                },
                {
                  "name": "888.888.888.888", "size": 86
                },
                {
                  "name": "888.888.888.888", "size": 77
                },
                {
                  "name": "888.888.888.888", "size": 72
                }
              ]
            },
            {
              "name": "US2",
              "children": [
                {
                  "name": "555.555.555.555", "size": 37
                },
                {
                  "name": "666.666.666.666", "size": 24
                },
                {
                  "name": "777.777.777.777", "size": 7
                },
                {
                  "name": "888.888.888.888", "size": 5
                },
                {
                  "name": "888.888.888.888", "size": 5
                },
                {
                  "name": "888.888.888.888", "size": 4
                },
                {
                  "name": "888.888.888.888", "size": 3
                },
                {
                  "name": "888.888.888.888", "size": 2
                },
                {
                  "name": "888.888.888.888", "size": 1
                },
                {
                  "name": "888.888.888.888", "size": 1
                }
              ]
            },
            {
              "name": "REST2",
              "children": [
                {
                  "name": "555.555.555.555", "size": 500
                }
              ]
            }
          ]
        },
        {
          "name": "world2",
          "children": [
            {
              "name": "SK",
              "children": [
                {
                  "name": "111.111.111.111", "size": 388
                },
                {
                  "name": "222.222.222.222", "size": 377
                },
                {
                  "name": "333.333.333.333", "size": 111
                },
                {
                  "name": "444.444.444.444", "size": 100
                }
              ]
            },
            {
              "name": "US",
              "children": [
                {
                  "name": "555.555.555.555", "size": 37
                },
                {
                  "name": "666.666.666.666", "size": 24
                },
                {
                  "name": "777.777.777.777", "size": 73
                },
                {
                  "name": "888.888.888.888", "size": 53
                },
                {
                  "name": "888.888.888.888", "size": 53
                }
              ]
            },
            {
              "name": "REST",
              "children": [
                {
                  "name": "555.555.555.555", "size": 400
                }
              ]
            }
          ]
        }
      ]
    }

    this.dataNew = this.data;

    let index = 0;
    let rest = 0;
    for (let i = 0; i < this.data.children.length; i++) {
      let tmp = this.data.children[i];
      let count = 0;
      for (let j = 0; j < tmp.children.length; j++) {
        count += tmp.children[j].size;
      }
      if (count > 30000000) {
        this.dataNew.children[index] = tmp;
        index++;
      }
      else {
        rest += count;
      }
    }
    this.dataNew.children[index + 1] = {
      "name": "REST",
      "children": [
        {
          "name": "REST", "size": rest * 2
        }
      ]
    };
    this.dataNew.children = this.dataNew.children.slice(0, index + 2);

    console.log("data", data);
    console.log("dataNew", this.dataNew);*/

    var width = 950;
    var height = 600;
    var radius = Math.min(width, height) / 2;
    var color = d3.scaleOrdinal(d3.schemeCategory10);

    var b = {
      w: 160, h: 50, s: 3, t: 20
    };

    const colors = ['green', 'red', 'yellow', 'blue', 'brown'];

    var totalSize = 0;

    var g = d3.select('#clock')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');


    var partition = d3.partition()
      .size([2 * Math.PI, radius * radius]);

    var arc = d3.arc()
      .startAngle(function (d) { return d.x0; })
      .endAngle(function (d) { return d.x1; })
      .innerRadius(function (d) { return Math.sqrt(d.y0); })
      .outerRadius(function (d) { return Math.sqrt(d.y1); });

    createVisualization(this.data);

    function createVisualization(json) {

      initializeBreadcrumbTrail();

      g.append("svg:circle")
        .attr("r", radius)
        .style("opacity", 0);

      var root = d3.hierarchy(json)
        .sum(function (d) { return d.size; })
        .sort(function (a, b) { return b.value - a.value; });

      var nodes = partition(root).descendants()
        .filter(function (d) {
          return (d.x1 - d.x0 > 0.005); 
        });

      var path = g.data([json]).selectAll("path")
        .data(nodes)
        .enter().append("svg:path")
        .attr("display", function (d) { return d.depth ? null : "none"; })
        .attr("d", arc)
        .attr("fill-rule", "evenodd")
        .style("fill", function (d) { return color((d.children ? d : d.parent).data.name) })
        .style("opacity", 1)
        .on("mouseover", mouseover);

      d3.select("#container").on("mouseleave", mouseleave);

      totalSize = path.datum().value;
    };

    function mouseover(d) {

      var percentage = (100 * d.value / totalSize).toPrecision(3);
      var percentageString = percentage + "%";
      if (parseInt(percentage) < 0.1) {
        percentageString = "< 0.1%";
      }

      d3.select("#percentage")
        .text(percentageString);

      d3.select("#explanation")
        .style("visibility", "");

      var sequenceArray = d.ancestors().reverse();
      sequenceArray.shift(); 
      updateBreadcrumbs(sequenceArray, percentageString);

      d3.selectAll("path")
        .style("opacity", 0.3);

      g.selectAll("path")
        .filter(function (node) {
          return (sequenceArray.indexOf(node) >= 0);
        })
        .style("opacity", 1);
    }

    function mouseleave(d) {

      d3.select("#trail")
        .style("visibility", "hidden");

      d3.selectAll("path").on("mouseover", null);

      d3.selectAll("path")
        .transition()
        .duration(1000)
        .style("opacity", 1)
        .on("end", function () {
          d3.select(this).on("mouseover", mouseover);
        });

      d3.select("#explanation")
        .style("visibility", "hidden");
    }

    function initializeBreadcrumbTrail() {
      var trail = d3.select("#sequence").append("svg:svg")
        .attr("width", width)
        .attr("height", 50)
        .attr("id", "trail");

      trail.append("svg:text")
        .attr("id", "endlabel")
        .style("fill", "#000");
    }

    function breadcrumbPoints(d, i) {
      var points = [];
      points.push("0,0");
      points.push(b.w + ",0");
      points.push(b.w + b.t + "," + (b.h / 2));
      points.push(b.w + "," + b.h);
      points.push("0," + b.h);
      if (i > 0) { 
        points.push(b.t + "," + (b.h / 2));
      }
      return points.join(" ");
    }

    function updateBreadcrumbs(nodeArray, percentageString) {
      var trail = d3.select("#trail")
        .selectAll("g")
        .data(nodeArray, function (d) { return d.data.name + d.depth; });

      trail.exit().remove();

      var entering = trail.enter().append("svg:g");

      entering.append("svg:polygon")
        .attr("points", breadcrumbPoints)
        .style("fill", function (d) { return color((d.children ? d : d.parent).data.name); });

      entering.append("svg:text")
        .attr("x", (b.w + b.t) / 2)
        .attr("y", b.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .style("font-size", "20px")
        .text(function (d) { return d.data.name; });

      entering.merge(trail).attr("transform", function (d, i) {
        return "translate(" + i * (b.w + b.s) + ", 0)";
      });

      d3.select("#trail").select("#endlabel")
        .attr("x", (nodeArray.length + 0.35) * (b.w + b.s))
        .attr("y", b.h / 2)
        .attr("dy", "0.35em")
        .attr("text-anchor", "middle")
        .style("font-size", "20px")
        .text(percentageString);

      d3.select("#trail")
        .style("visibility", "");
    }
  }
}
