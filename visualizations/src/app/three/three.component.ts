import { Component, ViewChild, Input, ElementRef, ChangeDetectorRef, OnInit, Renderer2 } from '@angular/core';
import * as THREE from 'three';
import * as Mappa from 'mappa-mundi'
import * as d3 from 'd3'
import * as dat from 'dat.gui'
import { ScaleBand, ScaleLinear } from 'd3';
import { BaseChartComponent } from '../base-chart.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, concatMap, switchMap, tap, take } from 'rxjs/operators';
import { State } from '../state';
import { Subject, concat } from 'rxjs';
import { MatOption } from '@angular/material/core';


interface Data {
  _id: {
    sl: [number, number], sc: string
  };
  byt: number;
  pkt: number;
}

@Component({
  selector: 'app-three',
  templateUrl: './three.component.html',
  styleUrls: ['./three.component.scss']
})
export class ThreeComponent extends BaseChartComponent implements OnInit {
  tz: number = 10;
  @ViewChild("myCanvas") myCanvas: any;
  @ViewChild('allSelected') private allSelected: MatOption;
  selectedClients = []

  private hslToRgb(h, s, l) {
    var r, g, b;

    if (s == 0) {
      r = g = b = l; // achromatic
    } else {
      var hue2rgb = function hue2rgb(p, q, t) {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      }

      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }

    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
  }

  private componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
  }

  private numberToColorHsl(i, min, max) {
    var ratio = i;
    if (min > 0 || max < 1) {
      if (i < min) {
        ratio = 0;
      } else if (i > max) {
        ratio = 1;
      } else {
        var range = max - min;
        ratio = (i - min) / range;
      }
    }

    var hue = ratio * 1.2 / 3.60;

    var rgb = this.hslToRgb(hue, 1, .5);

    return "#" + this.componentToHex(rgb[0]) + this.componentToHex(rgb[1]) + this.componentToHex(rgb[2]);
  }

  private VIEW_ANGLE = 60;
  private NEAR = 1;
  private FAR = 10000;
  private WIDTH;
  private HEIGHT;

  private scene: THREE.Scene;
  private camera: THREE.Camera;
  private renderer: THREE.WebGLRenderer;
  private myMap;
  private inputValue = 0;

  private pickedMesh;
  private startLat;
  private startLong;

  private sprite;
  private spritePackets;
  private meshesPackets1;
  private meshesPackets2;
  private meshesPackets3;
  private meshesPackets4;

  private xx: any[];

  setup: boolean = true;
  coordinates: boolean = true;
  picked: boolean = false;

  public data: any[]
  private currentOption = "Coordinates";
  private selectedOption = "Coordinates";
  private meshInfo;
  currentHour;

  hourChange$ = new Subject<number>()
  jsons = [
    {
      viewValue: '14:00 - 15:00',
      value: 14
    }, {
      viewValue: '15:00 - 16:00',
      value: 15
    },
    {
      viewValue: '16:00 - 17:00',
      value: 16
    }, {
      viewValue: '17:00 - 18:00',
      value: 17
    }, {
      viewValue: '18:00 - 19:00',
      value: 18
    },
    {
      viewValue: '19:00 - 20:00',
      value: 19
    },
    {
      viewValue: '20:00 - 21:00',
      value: 20
    }
  ]
  clientIps: []
  clientIpsS: []

  /*jsons = [
    {
      viewValue: '09:00 - 10:00',
      value: 9
    }, {
      viewValue: '10:00 - 11:00',
      value: 10
    },
    {
      viewValue: '11:00 - 12:00',
      value: 11
    }, 
    {
      viewValue: '12:00 - 13:00',
      value: 12
    }, 
    {
      viewValue: '13:00 - 14:00',
      value: 13
    },
    {
      viewValue: '14:00 - 15:00',
      value: 14
    },
    {
      viewValue: '15:00 - 16:00',
      value: 15
    },
    {
      viewValue: '16:00 - 17:00',
      value: 16
    }
  ]*/

  coordChange$ = new Subject<number>()
  coords = [
    {
      viewValue: 'Coordinates',
      value: 1
    }, {
      viewValue: 'States',
      value: 2
    }
  ]

  onValueChange(event) {
    this.hourChange$.next(event.value)
  }

  onValueChangeCoords(event) {
    if (event.value === 1) {
      this.selectedOption = "Coordinates";
    }
    else {
      this.selectedOption = "States";
    }
    this.setup = false;
  }

  onSunburst() {
    const modal = document.getElementById("myModal");
    modal.style.display = "block";

    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  onTimezone() {
    const modal = document.getElementById("timeModal");
    modal.style.display = "block";

    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  onPB() {
    const modal = document.getElementById("pbModal");
    modal.style.display = "block";

    window.onclick = function (event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
  }

  onKey(event) { this.inputValue = event.target.value; }

  onUpdate() {
    this.resetScene();
  }

  private resetScene() {
    for (let mesh of this.meshes) {
      this.scene.remove(mesh);
    }

    for (let curve of this.curves) {
      this.scene.remove(curve);
    }

    for (let sphere of this.spheres) {
      this.scene.remove(sphere);
    }

    this.scene.remove(this.pickedMesh);

    this.setup = false;
    this.picked = false;
  }

  private meshes = [];
  private curves = [];
  private spheres = [];
  private latitude = [];
  private longitude = [];
  private clientLatitude = [];
  private clientLongitude = [];

  private setupMeshes() {
    this.meshes = [];
    this.latitude = [];
    this.longitude = [];

    const geometry = new THREE.ConeGeometry(5, 20, 32);

    for (let i = 0; i < this.meshInfo.length; i++) {
      const current = this.meshInfo[i];

      if (current._id.sl.length === 2 && current.pkt > 50 && current.byt / 1000000 > this.inputValue && current._id.sl[1] != 0) {
        const colorValue = current.byt / 1000000 > 10 ? 0 : (1 - Math.abs((current.byt / 1000000) / 10));
        const mesh = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
        const scale = current.pkt > 1000 ? 0.65 : current.pkt / 1538;
        mesh.scale.set(scale, scale, scale);
        mesh.userData = [current._id.sl[1], current._id.sl[0], current.byt / 1000000, current._id.sc];
        this.meshes.push(mesh);

        this.latitude.push(current._id.sl[1]);
        this.longitude.push(current._id.sl[0]);
      }
    }
  }

  private setupStateMeshes() {
    this.meshes = [];
    this.latitude = [];
    this.longitude = [];

    const geometry = new THREE.ConeGeometry(5, 20, 32);

    for (let i = 0; i < this.countries.length; i++) {
      if (this.countries[i].byt * 1000 > this.inputValue) {
        const colorValue = this.countries[i].byt * 1000 > 100 ? 0 : (1 - Math.abs((this.countries[i].byt * 1000) / 100));
        const mesh = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
        let scale = 0.8;
        mesh.scale.set(scale, scale, scale);

        let lat;
        let long;
        let searchVal = this.countries[i]._id;
        if (searchVal == "REST") {
          lat = 0;
          long = 0;
        }
        else {
          for (var j = 0; j < this.countriesInfo.length; j++) {
            if (this.countriesInfo[j].country_code == searchVal) {
              lat = this.countriesInfo[j].latlng[0];
              long = this.countriesInfo[j].latlng[1];
            }
          }
          if (lat == null || long == null) {
            lat = 0;
            long = 0;
          }
        }
        if (lat != 0) {
          mesh.userData = [lat, long, this.countries[i].byt * 1000, searchVal];
          this.meshes.push(mesh);

          this.latitude.push(lat);
          this.longitude.push(long);
        }
      }
    }
  }

  constructor(protected elRef: ElementRef, private store: State, protected cd: ChangeDetectorRef, private httpClient: HttpClient) {
    super(elRef, store, cd)
  }

  private countriesInfo;
  private countries;
  async ngOnInit() {
    const data = await require('./countries.json');
    this.countriesInfo = data;

    this.hourChange$.pipe(concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/bar-data',
      {
        start: `2020-02-19T${v < 10 ? '0' + v : v}:00:00+00:00`,
        end: `2020-02-19T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`
      }
    ))).subscribe(data => {
      console.log('bar', data)
      const less = data.filter(({ byt }) => byt < 0.01)
        .reduce((prev, { byt }) => ({ ...prev, byt: prev.byt + byt }), { _id: 'REST', byt: 0 })
      const more = data.filter(({ byt }) => byt >= 0.01);
      this.data = [...more, less];
      this.countries = this.data;
    })

    this.hourChange$.pipe(
      tap(v => {
        this.currentHour = v
        d3.selectAll("svg > *").remove()
      }),
      concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/mongo-test',
        {
          start: `2020-02-19T${v < 10 ? '0' + v : v}:00:00+00:00`,
          end: `2020-02-19T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`
        }
      ))).subscribe(d => {
        console.log(d);
        this.meshInfo = d
        this.store.patchData({ data: d });
        this.data = d
        this.setup = false;
        this.picked = false;
      })
    this.hourChange$.next(14)
  }

  ngAfterViewInit(): void {
    this.renderScene();
  }

  formatHourLabel(value: number) {
    return `${value}:00-${value + 1}:00`;
  }

  private renderScene() {
    this.WIDTH = 1400;
    this.HEIGHT = 1100;
    const ASPECT = this.WIDTH / this.HEIGHT;

    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(this.VIEW_ANGLE, ASPECT, this.NEAR, this.FAR);
    const raycaster = new THREE.Raycaster();
    const mouse = new THREE.Vector2();

    this.renderer = new THREE.WebGLRenderer({ alpha: true, canvas: this.myCanvas.nativeElement });

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.domElement.addEventListener('click', caster.bind(this), false);

    this.camera.position.z = 300;
    this.scene.add(this.camera);
    this.renderer.setSize(this.WIDTH, this.HEIGHT);

    const state = this.store;
    function caster(e) {
      mouse.x = (e.offsetX / this.WIDTH) * 2 - 1;
      mouse.y = - (e.offsetY / this.HEIGHT) * 2 + 1;

      raycaster.setFromCamera(mouse, this.camera);

      var intersects = raycaster.intersectObjects(this.scene.children, true);

      mouse.x = (e.offsetX / this.WIDTH) * 2 - 1;
      mouse.y = - (e.offsetY / this.HEIGHT) * 2 + 1;

      raycaster.setFromCamera(mouse, this.camera);

      var intersects = raycaster.intersectObjects(this.scene.children);
      if (intersects.length > 0) {
        const data = intersects[0].object.userData;
        const mb = data[2];

        const lat = data[0];
        const long = data[1];

        if (lat && long && data[3] != "C") {
          if (this.selectedOption == "Coordinates") {
            const modal = document.getElementById("providerModal");
            const code = data[3]
            // modal.style.display = "block";
            document.getElementById("p1").innerHTML = lat;
            document.getElementById("p2").innerHTML = long;
            document.getElementById("p3").innerHTML = mb;

            this.startLat = lat;
            this.startLong = long;

            // This removes all meshes except the picked one
            for (let mesh of this.meshes) {
              this.scene.remove(mesh);
            }

            this.meshes = [];
            this.latitude = [];
            this.longitude = [];
            this.megaBytes = [];
            this.clientLatitude = [];
            this.clientLongitude = [];

            this.pickedMesh = intersects[0].object;
            this.picked = true;
            this.httpClient.post('http://localhost:3002/test/detail',
              {
                start: `2020-02-19T${this.currentHour < 10 ? '0' + this.currentHour : this.currentHour}:00:00+00:00`,
                end: `2020-02-19T${this.currentHour + 1 < 10 ? '0' + (this.currentHour + 1) : this.currentHour + 1}:00:00+00:00`,
                sl: [this.startLong, this.startLat],
                sc: code
              }
            ).pipe(take(1)).subscribe(data => {
              // data[0].clients.forEach(element => {
              //   this.clientLatitude.push(element[1]);
              //   this.clientLongitude.push(element[0]);
              // });
              this.clientIps = data[0].ips;
              this.selectedClients = [...this.clientIps]
              modal.style.display = "block";
            })

            window.onclick = function (event) {
              if (event.target == modal) {
                this.clientIps = []
                this.selectedClients = []
                modal.style.display = "none";
              }
            }
          }
          else {
            const data = intersects[0].object.userData;
            const modal = document.getElementById("providerModal2");
            document.getElementById("p4").innerHTML = data[3];
            modal.style.display = "block";

            window.onclick = function (event) {
              if (event.target == modal) {
                this.clientIpsS = []
                this.selectedClients = []
                modal.style.display = "none";
              }
            }
          }

        }
        else if (lat && long && data[3] == "C") {
          if (this.selectedOption == "Coordinates") {
            var nest = [];
            var keys = [];
            var vals = [];

            for (let l = 0; l < this.clientLatitude.length; l++) {
              if (l < 20) {
                if (lat == this.clientLatitude[l] && long == this.clientLongitude[l]) {
                  keys = Object.keys(this.xx[l].clients);
                  vals = Object.values(this.xx[l].clients);
                  break;
                }
              }
            }

            keys.forEach(function (d, i) {
              var obj = {
                _id: d,
                byt: vals[i]
              }
              nest.push(obj);
            })

            var margin = { top: 20, right: 20, bottom: 30, left: 40 },
              width = 960 - margin.left - margin.right,
              height = 500 - margin.top - margin.bottom;

            var x = d3.scaleBand()
              .range([0, width])
              .padding(0.1);
            var y = d3.scaleLinear()
              .range([height, 0]);

            var svg = d3.select("#aa").append("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
              .append("g")
              .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

            // Scale the range of the data in the domains
            x.domain(nest.map(function (d) { return d._id; }));
            y.domain([0, d3.max(nest, function (d) { return d.byt; })]).nice();

            // append the rectangles for the bar chart
            svg.selectAll(".bar")
              .data(nest)
              .enter().append("rect")
              .attr("class", "bar")
              .attr("x", function (d) { return x(d._id); })
              .attr("width", x.bandwidth())
              .attr("y", function (d) { return y(d.byt); })
              .attr("height", function (d) { return height - y(d.byt); });

            // add the x Axis
            svg.append("g")
              .attr("transform", "translate(0," + height + ")")
              .call(d3.axisBottom(x));

            // add the y Axis
            svg.append("g")
              .call(d3.axisLeft(y).ticks(3));

            const modal = document.getElementById("clientModal");
            modal.style.display = "block";

            window.onclick = function (event) {
              if (event.target == modal) {
                modal.style.display = "none";
              }
            }
          }
        }
      }

      this.update();
    }

    const light = new THREE.PointLight(0xffffff, 1.2);
    light.position.set(0, 0, 20);
    this.scene.add(light);

    const key =
      'pk.eyJ1IjoibWVyZXRoIiwiYSI6ImNqbXA3Z2ZoNzB4YzIzeHFjb3VwdXJuN2oifQ.LlSXvjk8lCZDBl6OYq0xdA';
    const options = {
      lat: 0,
      lng: 0,
      zoom: 4,
      style: 'mapbox://styles/mapbox/traffic-night-v2',
      pitch: 50,
    };

    const mappa = new Mappa('MapboxGL', key);
    this.myMap = mappa.tileMap(options);
    this.myMap.overlay(this.myCanvas.nativeElement);
    this.myMap.onChange(this.update.bind(this));

    this.update();
  }

  private update() {
    if (this.selectedOption != this.currentOption) {
      this.currentOption = this.selectedOption;
      this.setup = false;
    }

    if (!this.setup && this.selectedOption === "Coordinates") {
      for (let mesh of this.meshes) {
        this.scene.remove(mesh);
      }
      for (let curve of this.curves) {
        this.scene.remove(curve);
      }
      this.scene.remove(this.pickedMesh);
      this.scene.remove(this.sprite);

      const map = new THREE.TextureLoader().load("assets/legend10.PNG");
      const material = new THREE.SpriteMaterial({ map: map, color: 0xffffff });
      this.sprite = new THREE.Sprite(material);
      this.scene.add(this.sprite);

      const imageWidth = 90;
      const imageHeight = 25;

      this.sprite.scale.set(imageWidth, imageHeight, 1);
      this.sprite.position.set(-175, -150, 0);

      this.setupMeshes();
      this.addMeshes();
      this.addPacketsLegend();
      this.setup = true;
    }
    else if (!this.setup && this.selectedOption === "States") {
      for (let mesh of this.meshes) {
        this.scene.remove(mesh);
      }
      for (let curve of this.curves) {
        this.scene.remove(curve);
      }
      this.scene.remove(this.pickedMesh);
      this.scene.remove(this.sprite);
      this.scene.remove(this.spritePackets);
      this.scene.remove(this.meshesPackets1);
      this.scene.remove(this.meshesPackets2);
      this.scene.remove(this.meshesPackets3);
      this.scene.remove(this.meshesPackets4);

      const map = new THREE.TextureLoader().load("assets/legend100.PNG");
      const material = new THREE.SpriteMaterial({ map: map, color: 0xffffff });
      this.sprite = new THREE.Sprite(material);
      this.scene.add(this.sprite);

      const imageWidth = 90;
      const imageHeight = 25;

      this.sprite.scale.set(imageWidth, imageHeight, 1);
      this.sprite.position.set(-175, -150, 0);

      this.setupStateMeshes();
      this.addMeshes();
      this.setup = true;
    }

    if (!this.picked) {
      this.showProviders();
    }
    else {
      this.showClients();
    }

    this.renderer.render(this.scene, this.camera);
  }

  private showProviders() {
    for (let i = 0; i < this.meshes.length; i++) {
      var pos = this.myMap.latLngToPixel(this.latitude[i], this.longitude[i]);

      var vector = new THREE.Vector3();
      vector.set((pos.x / this.WIDTH) * 2 - 1, -(pos.y / this.HEIGHT) * 2 + 1, 0.5);
      vector.unproject(this.camera);

      var dir = vector.sub(this.camera.position).normalize();
      var distance = -this.camera.position.z / dir.z;
      this.camera.position.clone().add(dir.multiplyScalar(distance));

      this.meshes[i].position.set(vector.x, vector.y, 0);
      this.meshes[i].rotation.set(-90, 0, 0);
    }
  }

  private addClients(code) {

  }

  private showClients() {
    for (let mesh of this.meshes) {
      this.scene.remove(mesh);
    }

    for (let curve of this.curves) {
      this.scene.remove(curve);
    }

    for (let sphere of this.spheres) {
      this.scene.remove(sphere);
    }

    this.scene.add(this.pickedMesh);

    this.showSpeheres();
    this.showCurves();

    var pos = this.myMap.latLngToPixel(this.startLat, this.startLong);

    var vector = new THREE.Vector3();
    vector.set((pos.x / this.WIDTH) * 2 - 1, -(pos.y / this.HEIGHT) * 2 + 1, 0.5);
    vector.unproject(this.camera);

    var dir = vector.sub(this.camera.position).normalize();
    var distance = -this.camera.position.z / dir.z;
    this.camera.position.clone().add(dir.multiplyScalar(distance));

    this.pickedMesh.position.set(vector.x, vector.y, 0);
    this.pickedMesh.rotation.set(-90, 0, 0);
  }

  private showCurves() {
    this.curves = [];

    for (let i = 0; i < this.clientLatitude.length; i++) {

      var startPos = this.myMap.latLngToPixel(this.startLat, this.startLong);
      var startVector = new THREE.Vector3();
      startVector.set((startPos.x / this.WIDTH) * 2 - 1, -(startPos.y / this.HEIGHT) * 2 + 1, 0.5);
      startVector.unproject(this.camera);

      var dir = startVector.sub(this.camera.position).normalize();
      var distance = -this.camera.position.z / dir.z;
      this.camera.position.clone().add(dir.multiplyScalar(distance));

      var clientPos = this.myMap.latLngToPixel(this.clientLatitude[i], this.clientLongitude[i]);
      var clientVector = new THREE.Vector3();
      clientVector.set((clientPos.x / this.WIDTH) * 2 - 1, -(clientPos.y / this.HEIGHT) * 2 + 1, 0.5);
      clientVector.unproject(this.camera);

      var clientDir = clientVector.sub(this.camera.position).normalize();
      var clientDistance = -this.camera.position.z / clientDir.z;
      this.camera.position.clone().add(clientDir.multiplyScalar(clientDistance));

      var material = new THREE.LineBasicMaterial({
        color: 0xff0000
      });

      var startMiddleX = startVector.x + 0.25 * (clientVector.x - startVector.x);
      var startMiddleY = startVector.y + 0.25 * (clientVector.y - startVector.y);

      var clientMiddleX = startVector.x + 0.75 * (clientVector.x - startVector.x);
      var clientMiddleY = startVector.y + 0.75 * (clientVector.y - startVector.y);

      var curve = new THREE.CubicBezierCurve3(
        new THREE.Vector3(startVector.x, startVector.y, 0),
        new THREE.Vector3(startMiddleX, startMiddleY, 50),
        new THREE.Vector3(clientMiddleX, clientMiddleY, 50),
        new THREE.Vector3(clientVector.x, clientVector.y, 0)
      );

      var points = curve.getPoints(50);

      var geometry = new THREE.BufferGeometry().setFromPoints(points);

      var newCurve = new THREE.Line(geometry, material);

      this.curves.push(newCurve);
    }

    this.addCurves();
  }

  private showSpeheres() {
    this.spheres = [];

    for (let i = 0; i < this.clientLatitude.length; i++) {

      var clientPos = this.myMap.latLngToPixel(this.clientLatitude[i], this.clientLongitude[i]);
      var clientVector = new THREE.Vector3();
      clientVector.set((clientPos.x / this.WIDTH) * 2 - 1, -(clientPos.y / this.HEIGHT) * 2 + 1, 0.5);
      clientVector.unproject(this.camera);

      var clientDir = clientVector.sub(this.camera.position).normalize();
      var clientDistance = -this.camera.position.z / clientDir.z;
      this.camera.position.clone().add(clientDir.multiplyScalar(clientDistance));

      var geometry = new THREE.SphereGeometry(5, 32, 32);
      var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
      var newSphere = new THREE.Mesh(geometry, material);
      newSphere.position.set(clientVector.x, clientVector.y, 0);
      newSphere.scale.set(0.28, 0.28, 0.28);

      newSphere.userData = [this.clientLatitude[i], this.clientLongitude[i], 1000, "C"];

      this.spheres.push(newSphere);
    }

    this.addSpheres();
  }

  private addMeshes() {
    for (let i = 0; i < this.meshes.length; i++) {
      this.scene.add(this.meshes[i]);
    }
  }

  private addCurves() {
    for (let i = 0; i < this.curves.length; i++) {
      this.scene.add(this.curves[i]);
    }
  }

  private addSpheres() {
    for (let i = 0; i < this.spheres.length; i++) {
      this.scene.add(this.spheres[i]);
    }
  }

  private addPacketsLegend() {
    const colorValue = 0.5;
    const geometry = new THREE.ConeGeometry(5, 20, 32);
    this.meshesPackets1 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
    let scale = 0.65;
    this.meshesPackets1.scale.set(scale, scale, scale);
    this.meshesPackets1.position.set(-135, -120, 10);
    this.meshesPackets1.rotation.set(Math.PI - 0.2, 0, 0);
    this.scene.add(this.meshesPackets1);

    this.meshesPackets2 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
    scale = 0.4876;
    this.meshesPackets2.scale.set(scale, scale, scale);
    this.meshesPackets2.position.set(-157, -120, 10);
    this.meshesPackets2.rotation.set(Math.PI - 0.2, 0, 0);
    this.scene.add(this.meshesPackets2);

    this.meshesPackets3 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
    scale = 0.3250;
    this.meshesPackets3.scale.set(scale, scale, scale);
    this.meshesPackets3.position.set(-179, -120, 10);
    this.meshesPackets3.rotation.set(Math.PI - 0.2, 0, 0);
    this.scene.add(this.meshesPackets3);

    this.meshesPackets4 = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: this.numberToColorHsl(colorValue, 0, 1), side: 2 }));
    scale = 0.1625;
    this.meshesPackets4.scale.set(scale, scale, scale);
    this.meshesPackets4.position.set(-201, -120, 10);
    this.meshesPackets4.rotation.set(Math.PI - 0.2, 0, 0);
    this.scene.add(this.meshesPackets4);

    const map = new THREE.TextureLoader().load("assets/packets.PNG");
    const material = new THREE.SpriteMaterial({ map: map, color: 0xffffff });
    this.spritePackets = new THREE.Sprite(material);
    this.scene.add(this.spritePackets);

    const imageWidth = 90;
    const imageHeight = 25;

    this.spritePackets.scale.set(imageWidth, imageHeight, 1);
    this.spritePackets.position.set(-175, -125, 0);
  }

  filterClients() {
    this.scene.remove(this.pickedMesh);
    this.scene.remove(this.sprite);
    this.scene.remove(this.spritePackets);
    this.scene.remove(this.meshesPackets1);
    this.scene.remove(this.meshesPackets2);
    this.scene.remove(this.meshesPackets3);
    this.scene.remove(this.meshesPackets4);
    this.httpClient.post<{ dl: [number, number], clients: { [key: string]: number }, byt: number }[]>('http://localhost:3002/test/clients', {
      ips: this.selectedClients,
      start: `2020-02-19T${this.currentHour < 10 ? '0' + this.currentHour : this.currentHour}:00:00+00:00`,
      end: `2020-02-19T${this.currentHour + 1 < 10 ? '0' + (this.currentHour + 1) : this.currentHour + 1}:00:00+00:00`,
    },
    ).subscribe(data => {
      this.xx = data;
      data.forEach(element => {
        if (element.dl[1] != 0) {
          this.clientLatitude.push(element.dl[1]);
          this.clientLongitude.push(element.dl[0]);
        }
        //this.xx[index] = Object.keys(element.clients);
        //this.yy[index] = Object.values(element.clients);
      });
    })
  }

  filterClientsS() {
    alert("filter");
  }
}
