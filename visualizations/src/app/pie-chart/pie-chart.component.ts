import { Component, ViewEncapsulation, OnInit, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy, Input, SimpleChanges, OnChanges } from '@angular/core';
import * as d3 from 'd3'
import { BaseChartComponent } from '../base-chart.component';
import { State } from '../state';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { take, concatMap } from 'rxjs/operators';

interface Data {
    _id: string;
    pkt: number;
}

@Component({
    selector: 'app-pie-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.scss'],
})
export class PieChartComponent extends BaseChartComponent implements OnInit {
    @Input() inputData: any[];
    @Input() hour$: Subject<number>
    title = 'Pie Chart';
    private data: Data[]

    private margin = { top: 20, right: 20, bottom: 30, left: 50 };
    private width: number;
    private height: number;
    private radius: number;

    constructor(protected elRef: ElementRef, protected state: State, protected cd: ChangeDetectorRef, private httpClient: HttpClient) {
        super(elRef, state, cd)
        this.width = 700 - this.margin.left - this.margin.right;
        this.height = 400 - this.margin.top - this.margin.bottom;
        this.radius = Math.min(this.width, this.height) / 2;
    }
    async ngOnInit() {
        super.ngOnInit()

        this.hour$.pipe(concatMap(v => this.httpClient.post<Data[]>('http://localhost:3002/test/pie-data',
            {
                start: `2020-02-19T${v < 10 ? '0' + v : v}:00:00+00:00`,
                end: `2020-02-19T${v + 1 < 10 ? '0' + (v + 1) : v + 1}:00:00+00:00`
            }
        ))).subscribe(data => {
            console.log('pie', data)
            const less = data.filter(({ pkt }) => pkt < 20000)
                .reduce((prev, { pkt }) => ({ ...prev, pkt: prev.pkt + pkt }), { _id: 'REST', pkt: 0 })
            const more = data.filter(({ pkt }) => pkt >= 20000);
            this.data = [...more, less];
            d3.select('#chart > svg').remove()
            this.drawPie();
        })

    }

    private drawPie() {
        const legendRectSize = 18;
        const legendSpacing = 4;

        const color = d3.scaleOrdinal().range(["#FF0000", "#009933" , "#0000FF", "#696969", "#bada55", "#7fe5f0", "#800080", "#ff80ed", "#407294", "#cbcba9", "#420420", "#133337", "#065535", "#c0c0c0", "#5ac18e", "#dcedc1", "#f7347a", "#003366", "#7fffd4", "#00ff00",]);;

        var svg: any;
        svg = d3.select('#chart')
            .append('svg')
            .attr('width', this.width)
            .attr('height', this.height)
            .append('g')
            .attr('transform', 'translate(' + (this.width / 2) +
                ',' + (this.height / 2) + ')');

        var arc: any;
        arc = d3.arc()
            .innerRadius(0)
            .outerRadius(this.radius);

        var pie: any;
        pie = d3.pie()
            .value(function (d: any) { return d.pkt; })
            .sort(null);

        var tooltip: any;
        tooltip = d3.select('#chart')
            .append('div')
            .attr('class', 'tooltip');

        tooltip.append('div')
            .attr('class', '_id');

        tooltip.append('div')
            .attr('class', 'pkt');

        var path: any;
        path = svg.selectAll('path')
            .data(pie(this.data))
            .enter()
            .append('path')
            .attr('d', arc)
            .attr('fill', function (d: any, i) {
                return color(d.data._id);
            });

        path.on('mouseover', function (d) {
            tooltip.select('._id').html(d.data._id);
            tooltip.select('.pkt').html(d.data.pkt + ' packets');
            tooltip.style('display', 'block');
        });

        path.on('mouseout', function () {
            tooltip.style('display', 'none');
        });

        path.on('mousemove', function (d) {
            tooltip.style('top', (d3.event.layerY + 10) + 'px')
                .style('left', (d3.event.layerX + 10) + 'px');
        });


        const legend = svg.selectAll('.legend')
            .data(color.domain())
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', function (d, i) {
                const height = legendRectSize + legendSpacing;
                const offset = height * color.domain().length / 2;
                const horz = -2 * legendRectSize;
                const vert = i * height - offset;
                return 'translate(' + -290 + ',' + vert + ')';
            });

        legend.append('rect')
            .attr('width', legendRectSize)
            .attr('height', legendRectSize)
            .style('fill', color)
            .style('stroke', color);

        legend.append('text')
            .attr('x', legendRectSize + legendSpacing)
            .attr('y', legendRectSize - legendSpacing)
            .text(function (d) { return d; });
    }
}
