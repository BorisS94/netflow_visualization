import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThreeComponent } from './three/three.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { ClockmapComponent } from './clockmap/clockmap.component';
import { LandingComponent } from './landing/landing.component';
import { AreaChartComponent } from './area-chart/area-chart.component';
import { SunburstComponent } from './sunburst/sunburst.component';
import { LegendComponent } from './legend/legend.component';
import { ScatterPlotComponent } from './scatter-plot/scatterplot.component';
import { LineClientsComponent } from './line-clients/line-clients.component';



export const ROUTES: Routes = [
  {
    path: 'Map visualization',
    component: ThreeComponent,
  },
  {
    path: 'Providers',
    component: SunburstComponent
  },
  {
    path: 'Attacks',
    component: AreaChartComponent
  },
  {
    path: 'Maintenance',
    component: LineChartComponent
  },
  {
    path: '',
    component: LandingComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
