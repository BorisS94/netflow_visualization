const elastic = require('@elastic/elasticsearch');
const { readdirSync } = require('fs');
const elasticClient = new elastic.Client({
  node: 'http://localhost:9200'
});
const insertData = async () => {
  // Checks if index allready exists
  const { body: isIndexCreated } = await elasticClient.indices.exists({
    index: 'data_15_03'
  });

  console.log('Index data_15_03 is allready created: ', isIndexCreated);

  // If not, create an index
  if (!isIndexCreated) {
    console.log('Creating new index');

    const {
      body: { acknowledged }
    } = await elasticClient.indices.create({
      index: 'data_15_03'
    });

    console.log('Index created: ', acknowledged);

    // If there is an error, end the whole method
    if (!acknowledged) {
      console.error('Failed to create index, aborting');

      return;
    }
  }
  async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  console.log('Begin inserting data to elasticsearch');
  // Insert each record to created index
  const files = readdirSync('/shared/extended/data_24_7_json').filter(f => f.match(/Copy of nf_dump_2020\.03\.15_\d{2}_\d{1,3}\.\d{1,3}\.\d{1,3}\.0\.json$/gm)).slice(42, 48);
  return asyncForEach(files, async (file) => {
    let docs = await require('/shared/extended/data_24_7_json/' + file)
    console.log('processing', file);
    docs = docs.map((d) => ({
      ...d,
      dl: d.dl && d.dl.length === 2 ? d.dl : [0, 0], // d.dl?.length === 2 ? d.dl : [0, 0]
      sl: d.sl && d.sl.length === 2 ? d.sl : [0, 0],
    }));
    //.flatMap(doc => [{ index: { _index: 'data_14_03' } }, doc])
    // Insert some documents
    for(let i = 0; i < docs.length; i+= 10000)
    {
	const body = docs.slice(i, i + 10000).flatMap(doc => [{ index: { _index: 'data_15_03' } }, doc]);
        console.log('inserting', i)
	await elasticClient.bulk({ refresh: true, body }).catch(err => console.error(err));
    }
    //console.log('inserting');
    //await elasticClient.bulk({ refresh: true, body }).catch(err => console.error(err));	
    //await asyncForEach(docs, (doc) => elasticClient.index({
    //  index: 'data_14_03',
    //  body: doc
    //}));
    console.log('inserted');
  })
};

(async () => {
  await insertData();
  console.log('Finished');
})();
