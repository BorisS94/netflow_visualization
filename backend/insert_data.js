const MongoClient = require('mongodb').MongoClient;
const { readFileSync, readdirSync } = require('fs')  
 async function asyncForEach(
  array,
  callback
) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
// Connection URL
const url = 'mongodb://10.147.20.92:27017';
 
// Database Name
const dbName = 'vd';
 const insertDocuments = async function(db) {
  // Get the documents collection
  const collection = db.collection('data_17_03');
  const files = readdirSync('/shared/extended/data_24_7_json').filter(f => f.match(/Copy of nf_dump_2020\.03\.17_\d{2}_\d{1,3}\.\d{1,3}\.\d{1,3}\.0\.json$/gm)).slice(0, 10);

  await asyncForEach(files, async (file) => {
    let docs = await require('/shared/extended/data_24_7_json/' + file)
    console.log('processing', file);
    docs = docs.map((d) => ({
      ...d,
      dl: d.dl && d.dl.length === 2 ? d.dl : [0, 0], // d.dl?.length === 2 ? d.dl : [0, 0]
      sl: d.sl && d.sl.length === 2 ? d.sl : [0, 0],
      te: new Date(d.te + '+0000'),
      ts: new Date(d.ts + '+0000')
    }));
    for(let i = 0; i < docs.length; i += 10000) {
       console.log('inserting', i)
       const toInsert = docs.slice(i, i + 10000);
       await collection.insertMany(toInsert);
    }
    // Insert some documents
    // console.log('inserting');
    // await asyncForEach(docs,(doc) => collection.insertOne(doc));
    console.log('inserted');
  })
}
// Use connect method to connect to the server
MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true }, async function(err, client) {
  console.log("Connected successfully to server");
 
  const db = client.db(dbName);
 
  await insertDocuments(db);
  client.close()
});
