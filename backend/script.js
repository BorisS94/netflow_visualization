const neatCsv = require('neat-csv');
const { extname, dirname } = require('path');
const { readdirSync, readFile, writeFileSync } = require('fs');
const source = '/shared/data/data_24_7';

const files = readdirSync(source);
async function asyncForEach(
  array,
  callback
) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
const p = (name) =>
  new Promise((resolve, reject) => {
    const path = source + '/' + name;
    const fileName = path.replace(dirname(path), '').replace(extname(path), '');

    readFile(path, async (err, data) => {
      if (err) {
        console.error(err);
        reject(err);
      }
      const resolved = await neatCsv(data, {
        mapHeaders: ({ header }) => {
          switch (header) {
            case 'da_location':
              return 'dl';
            case 'sa_location':
              return 'sl';
            case 'da_country':
              return 'dc';
            case 'sa_country':
              return 'sc';
            default:
              return header;
          }
        },
        mapValues: ({ header, value }) => {
          switch (header) {
            case 'dl':
              return value
                .split(',')
                .map((c) => Math.round((+c + Number.EPSILON) * 100) / 100)
                .reverse();
            case 'sl':
              return value
                .split(',')
                .map((c) => Math.round((+c + Number.EPSILON) * 100) / 100)
                .reverse();
            case 'in':
              return +value;
            case 'out':
              return +value;
            case 'bpp':
              return +value;
            case 'bps':
              return +value;
            case 'pkt':
              return +value;
            case 'byt':
              return +value;
            case 'sp':
              return +value;
            case 'dp':
              return +value;
            default:
              return value;
          }
        },
      });
      console.log(resolved[0]);

      writeFileSync(
        source + '_json' + fileName + '.json',
        JSON.stringify(resolved)
      );
      resolve();
    });
  });

asyncForEach(files, p)
