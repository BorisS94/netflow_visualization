let docs = require('/home/jan/school/vd/dataset_json/nf_data_sh_144_16.json')
let docs2 = require('/home/jan/school/vd/dataset_json/nf_data_sh_145_16.json')
const { writeFileSync } = require('fs');


const set = new Map()
const subnets = new Set()
let ovrByt = 0;
for (const doc of docs) {
  if (doc.sa.match(/^109\.74\.144/)) {
    if (set.has(doc.da)) {
      set.set(doc.da, set.get(doc.da) + doc.byt)
    }
    else {
      set.set(doc.da, doc.byt)
    }
    ovrByt += doc.byt
    const sub = doc.da.split('.').slice(0, 3).join('.') + '.0'
    subnets.add(sub)
  }
  if (doc.da.match(/^109\.74\.144/)) {
    if (set.has(doc.sa)) {
      set.set(doc.sa, set.get(doc.sa) + doc.byt)
    }
    else {
      set.set(doc.sa, doc.byt)
    }
    ovrByt += doc.byt
  }
}

// console.log(subnets)
// console.log(subnets.size, set.size, ovrByt)

const match = []
for (const d of subnets) {
  const matchedSub = new RegExp('^' + d.split('.').slice(0, 3).join('\\.') + '\\.')
  const obj = {}
  obj.ips = Array.from(set.keys()).filter((key) => key.match(matchedSub)).map(k => ({ name: k, size: set.get(k) }))
  obj.sum = obj.ips.reduce((acc, curr) => acc + curr.size, 0)
  if (obj.sum >= 1000000) {
    match.push({ name: d + '/24', children: obj.ips, sum: obj.sum })
    // console.log(obj)
  }
}

const set2 = new Map()
const subnets2 = new Set()
let ovrByt2 = 0;
for (const doc of docs2) {
  if (doc.sa.match(/^109\.74\.145/)) {
    if (set2.has(doc.da)) {
      set2.set(doc.da, set.get(doc.da) + doc.byt)
    }
    else {
      set2.set(doc.da, doc.byt)
    }
    ovrByt2 += doc.byt
    const sub = doc.da.split('.').slice(0, 3).join('.') + '.0'
    subnets2.add(sub)
  }
  if (doc.da.match(/^109\.74\.145/)) {
    if (set2.has(doc.sa)) {
      set2.set(doc.sa, set.get(doc.sa) + doc.byt)
    }
    else {
      set2.set(doc.sa, doc.byt)
    }
    ovrByt2 += doc.byt
  }
}

// console.log(subnets2)
// console.log(subnets2.size, set2.size, ovrByt2)

const match2 = []
for (const d of subnets2) {
  const matchedSub = new RegExp('^' + d.split('.').slice(0, 3).join('\\.') + '\\.')
  const obj = {}
  obj.ips = Array.from(set2.keys()).filter((key) => key.match(matchedSub)).map(k => ({ name: k, size: set2.get(k) }))
  obj.sum = obj.ips.reduce((acc, curr) => acc + curr.size, 0)
  if (obj.sum >= 1000000) {
    match2.push({name: d + '/24', children: obj.ips, sum: obj.sum})
    // console.log(obj)
  }
}

console.log(match, Object.keys(match).length, ovrByt)
console.log(match2, Object.keys(match2).length, ovrByt2)

const result = { name: 'providers', children: [{ name: '109.74.144.0/24', children: match }, { name: '109.74.145.0/24', children: match2}]}

console.log(result.children[0].children[0])
writeFileSync(
  __dirname + '/src/netflow-module/providers_16.json',
  JSON.stringify(result)
);