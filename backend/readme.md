## NetFlow API gateway + core

`npm install`

`npm run serve`

<!-- import {
  connection,
  SchemaDefinition,
  Types,
  Mongoose,
  model,
  connect,
  Schema,
  Model,
} from 'mongoose';
import { toBuffer, toString } from 'ip';
import { BootstrapService } from '../core/common';

async function asyncForEach<T = any>(
  array: T[],
  callback: (value: T, index?: number, array?: T[]) => void
) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const schemaDef: SchemaDefinition = {
  dl: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ['Point'], // 'location.type' must be 'Point'
    },
    coordinates: {
      type: [Number],
    },
  },
  sl: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ['Point'], // 'location.type' must be 'Point'
    },
    coordinates: {
      type: [Number],
    },
  },
  sc: String,
  dc: String,
  flg: String,
  ts: Date,
  te: Date,
  in: Number,
  out: Number,
  bpp: Number,
  bps: Number,
  pkt: Number,
  byt: Number,
  sa: String,
  da: String,
  ra: String,
  pr: String,
  sp: Number,
  dp: Number,
  it: String,
  ic: String,
};
export class MongoService implements BootstrapService {
  private dataModel: Model<any>;

  async bootstrapService() {
    await connect('mongodb://localhost:27017/vd', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    connection.on('error', (err) => console.error(err));
    const schema = new Schema(schemaDef);
    this.dataModel = model('data', schema, 'data', false);
    for (let i = 14; i <= 20; i++) {
      await this.insert(i);
    }
  }

  async insert(i) {
    console.log(i);
    let docs = require(`/shared/data/nf_data_inet_${i}.json`);
    console.log('insert');
    docs = docs.map((d) => ({
      ...d,
      dl: { type: 'Point', coordinates: d.dl?.length === 2 ? d.dl : [0, 0] },
      sl: { type: 'Point', coordinates: d.sl?.length === 2 ? d.sl : [0, 0] },
    }));
    return asyncForEach(docs, (d) => {
      return this.dataModel.create(d);
    }).then(() => console.log('done'));
  }

  async aggregate() {
    const result = await this.dataModel
      .aggregate()
      .group({
        _id: '$sl.coordinates',
        byt: { $sum: '$byt' },
        ips: { $addToSet: '$sa' },
      })
      .sort({ byt: -1 }); // .exec((err, res) => {console.log(res.map(r => ({...r, _id: toString(r._id)})))})
    // return result
    return result;
  }
} -->
