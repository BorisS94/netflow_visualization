const MongoClient = require('mongodb').MongoClient;
// Connection URL
const url = 'mongodb://10.147.20.92:27017';

const dbName = 'vd';
const createIndexes = function (db) {
  // Get the documents collection
  const collection = db.collection('data_providers');
  return collection.createIndexes([{ key: { dl: '2dsphere' } }, { key: { sl: '2dsphere' } }, { key: { sc: 1 } }, { key: { dc: 1 } }, { key: { te: 1 } }, { key: { flg: 1 } }, { key: { sa: 1 } }, { key: { da: 1 } }])
}

MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true, connectTimeoutMS: 60*60*30, socketTimeoutMS: 60*60*30 }, async function (err, client) {
  console.log("Connected successfully to server");

  const db = client.db(dbName);

  await createIndexes(db);
  client.close()
});