const MongoClient = require('mongodb').MongoClient;
async function asyncForEach(
  array,
  callback
) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
// Connection URL
const url = 'mongodb://10.147.20.92:27017';
const GIGABYTE_DIVISION = 1000000000;
// Database Name
const dbName = 'vd';
const insertDocuments = async function (db) {
  // Get the documents collection
  const collection = db.collection('data_16_03');
  const insertCollection = db.collection('data_maintain')
  try {
    console.log('afoj')
    const cursor = await collection.aggregate([
      {
        $bucket: {
          groupBy: '$te',
          boundaries: [
            // new Date('2020-03-14T00:00:00+00:00'),
            // new Date('2020-03-14T01:00:00+00:00'),
            // new Date('2020-03-14T02:00:00+00:00'),
            // new Date('2020-03-14T03:00:00+00:00'),
            // new Date('2020-03-14T04:00:00+00:00'),
            // new Date('2020-03-14T05:00:00+00:00'),
            // new Date('2020-03-14T06:00:00+00:00'),
            // new Date('2020-03-14T07:00:00+00:00'),
            // new Date('2020-03-14T08:00:00+00:00'),
            // new Date('2020-03-14T09:00:00+00:00'),
            // new Date('2020-03-14T10:00:00+00:00'),
            // new Date('2020-03-14T11:00:00+00:00'),
            // new Date('2020-03-14T12:00:00+00:00'),
            // new Date('2020-03-14T13:00:00+00:00'),
            // new Date('2020-03-14T14:00:00+00:00'),
            // new Date('2020-03-14T15:00:00+00:00'),
            // new Date('2020-03-14T16:00:00+00:00'),
            // new Date('2020-03-14T17:00:00+00:00'),
            // new Date('2020-03-14T18:00:00+00:00'),
            // new Date('2020-03-14T19:00:00+00:00'),
            // new Date('2020-03-14T20:00:00+00:00'),
            // new Date('2020-03-14T21:00:00+00:00'),
            // new Date('2020-03-14T22:00:00+00:00'),
            // new Date('2020-03-14T23:00:00+00:00'),
            // new Date('2020-03-15T00:00:00+00:00'),
            // new Date('2020-03-15T01:00:00+00:00'),
            // new Date('2020-03-15T02:00:00+00:00'),
            // new Date('2020-03-15T03:00:00+00:00'),
            // new Date('2020-03-15T04:00:00+00:00'),
            // new Date('2020-03-15T05:00:00+00:00'),
            // new Date('2020-03-15T06:00:00+00:00'),
            // new Date('2020-03-15T07:00:00+00:00'),
            // new Date('2020-03-15T08:00:00+00:00'),
            // new Date('2020-03-15T09:00:00+00:00'),
            // new Date('2020-03-15T10:00:00+00:00'),
            // new Date('2020-03-15T11:00:00+00:00'),
            // new Date('2020-03-15T12:00:00+00:00'),
            // new Date('2020-03-15T13:00:00+00:00'),
            // new Date('2020-03-15T14:00:00+00:00'),
            // new Date('2020-03-15T15:00:00+00:00'),
            // new Date('2020-03-15T16:00:00+00:00'),
            // new Date('2020-03-15T17:00:00+00:00'),
            // new Date('2020-03-15T18:00:00+00:00'),
            // new Date('2020-03-15T19:00:00+00:00'),
            // new Date('2020-03-15T20:00:00+00:00'),
            // new Date('2020-03-15T21:00:00+00:00'),
            // new Date('2020-03-15T22:00:00+00:00'),
            // new Date('2020-03-15T23:00:00+00:00'),
            new Date('2020-03-16T00:00:00+00:00'),
            new Date('2020-03-16T01:00:00+00:00'),
            new Date('2020-03-16T02:00:00+00:00'),
            new Date('2020-03-16T03:00:00+00:00'),
            new Date('2020-03-16T04:00:00+00:00'),
            new Date('2020-03-16T05:00:00+00:00'),
            new Date('2020-03-16T06:00:00+00:00'),
            new Date('2020-03-16T07:00:00+00:00'),
            new Date('2020-03-16T08:00:00+00:00'),
            new Date('2020-03-16T09:00:00+00:00'),
            new Date('2020-03-16T10:00:00+00:00'),
            new Date('2020-03-16T11:00:00+00:00'),
            new Date('2020-03-16T12:00:00+00:00'),
            new Date('2020-03-16T13:00:00+00:00'),
            new Date('2020-03-16T14:00:00+00:00'),
            new Date('2020-03-16T15:00:00+00:00'),
            new Date('2020-03-16T16:00:00+00:00'),
            new Date('2020-03-16T17:00:00+00:00'),
            new Date('2020-03-16T18:00:00+00:00'),
            new Date('2020-03-16T19:00:00+00:00'),
            new Date('2020-03-16T20:00:00+00:00'),
            new Date('2020-03-16T21:00:00+00:00'),
            new Date('2020-03-16T22:00:00+00:00'),
            new Date('2020-03-16T23:00:00+00:00'),
            new Date('2020-03-17T00:00:00+00:00')
          ],
          default: 'other',
          output: {
            byt: {
              $sum: { $divide: ['$byt', GIGABYTE_DIVISION] },
            },
            bpp: { $sum: '$bpp' },
            bps: { $sum: '$bps' },
            pkt: { $sum: '$pkt' },
          },
        },
      }
    ])
    const data = (await cursor.toArray()).map(({_id, ...rest}) => ({...rest, te: _id}))
    console.log(data)
    await insertCollection.insertMany(data);
  }
  catch (err) {
    console.log('err', err)
  }
  // Insert some documents
  // console.log('inserting');
  // await asyncForEach(docs,(doc) => collection.insertOne(doc));
}
// Use connect method to connect to the server
MongoClient.connect(url, { useUnifiedTopology: true, useNewUrlParser: true, connectTimeoutMS: 30 * 60 * 1000, socketTimeoutMS: 30 * 60 * 1000 }, async function (err, client) {
  console.log("Connected successfully to server");

  const db = client.db(dbName);

  await insertDocuments(db);
  client.close()
});
