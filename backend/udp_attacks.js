const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

/*
 * Requires the MongoDB Node.js Driver
 * https://mongodb.github.io/node-mongodb-native
 */

const agg = (start, end) => [
  {
    '$match': {
      'te': {
        '$gte': start,
        '$lt': end
      },
      'pr': 'UDP'
    }
  }, {
    '$group': {
      '_id': '$da',
      'count': {
        '$sum': 1
      },
      'pkt': {
        '$sum': '$pkt'
      },
      'ips': {
        '$addToSet': '$sa'
      },
      'ports': {
        '$addToSet': '$dp'
      },
      'sc': {
        '$addToSet': '$sc'
      }
    }
  }, {
    '$addFields': {
      'ips_length': {
        '$size': '$ips'
      },
      'ports_length': {
        '$size': '$ports'
      }
    }
  }, {
    '$sort': {
      'count': -1,
      'ports_length': -1
    }
  }
];

const bounds = [
          new Date('2020-03-14T00:00:00+00:00'),
        new Date('2020-03-14T01:00:00+00:00'),
        new Date('2020-03-14T02:00:00+00:00'),
        new Date('2020-03-14T03:00:00+00:00'),
        new Date('2020-03-14T04:00:00+00:00'),
        new Date('2020-03-14T05:00:00+00:00'),
        new Date('2020-03-14T06:00:00+00:00'),
        new Date('2020-03-14T07:00:00+00:00'),
        new Date('2020-03-14T08:00:00+00:00'),
        new Date('2020-03-14T09:00:00+00:00'),
        new Date('2020-03-14T10:00:00+00:00'),
        new Date('2020-03-14T11:00:00+00:00'),
        new Date('2020-03-14T12:00:00+00:00'),
        new Date('2020-03-14T13:00:00+00:00'),
        new Date('2020-03-14T14:00:00+00:00'),
        new Date('2020-03-14T15:00:00+00:00'),
        new Date('2020-03-14T16:00:00+00:00'),
        new Date('2020-03-14T17:00:00+00:00'),
        new Date('2020-03-14T18:00:00+00:00'),
        new Date('2020-03-14T19:00:00+00:00'),
        new Date('2020-03-14T20:00:00+00:00'),
        new Date('2020-03-14T21:00:00+00:00'),
        new Date('2020-03-14T22:00:00+00:00'),
        new Date('2020-03-14T23:00:00+00:00'),
        new Date('2020-03-15T00:00:00+00:00'),
        new Date('2020-03-15T01:00:00+00:00'),
        new Date('2020-03-15T02:00:00+00:00'),
        new Date('2020-03-15T03:00:00+00:00'),
        new Date('2020-03-15T04:00:00+00:00'),
        new Date('2020-03-15T05:00:00+00:00'),
        new Date('2020-03-15T06:00:00+00:00'),
        new Date('2020-03-15T07:00:00+00:00'),
        new Date('2020-03-15T08:00:00+00:00'),
        new Date('2020-03-15T09:00:00+00:00'),
        new Date('2020-03-15T10:00:00+00:00'),
        new Date('2020-03-15T11:00:00+00:00'),
        new Date('2020-03-15T12:00:00+00:00'),
        new Date('2020-03-15T13:00:00+00:00'),
        new Date('2020-03-15T14:00:00+00:00'),
        new Date('2020-03-15T15:00:00+00:00'),
        new Date('2020-03-15T16:00:00+00:00'),
        new Date('2020-03-15T17:00:00+00:00'),
        new Date('2020-03-15T18:00:00+00:00'),
        new Date('2020-03-15T19:00:00+00:00'),
        new Date('2020-03-15T20:00:00+00:00'),
        new Date('2020-03-15T21:00:00+00:00'),
        new Date('2020-03-15T22:00:00+00:00'),
        new Date('2020-03-15T23:00:00+00:00'),
        new Date('2020-03-16T00:00:00+00:00')
]

MongoClient.connect(
  'mongodb://10.147.20.92:27017',
  { useNewUrlParser: true, useUnifiedTopology: true },
  async function (connectErr, client) {
    assert.equal(null, connectErr);
    const coll = client.db('vd').collection('data_all');
    const insColl = client.db('vd').collection('data_attacks')

    for (let i = 0; i < bounds.length - 1; i++) {
      const cursro = coll.aggregate(agg(bounds[i], bounds[i + 1]));
      const data = await cursro.toArray()
      console.log(data);
      for (let j = 0; j < data.length; j += 10000) {
        console.log('inserting', i)
        const toInsert = data.slice(i, i + 10000).map(({_id, ...rest}) => ({te: bounds[i], ...rest}));
        await insColl.insertMany(toInsert);
      }
    }

    client.close();
  });