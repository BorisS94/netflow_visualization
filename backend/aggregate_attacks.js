const MongoClient = require('mongodb').MongoClient;

/*
 * Requires the MongoDB Node.js Driver
 * https://mongodb.github.io/node-mongodb-native
 */

const agg = [
  {
    '$match': {
      // 'te': {
      //   '$gte': new Date('Sat, 14 Mar 2020 00:00:00 GMT'),
      //   '$lt': new Date('Sat, 14 Mar 2020 01:00:00 GMT')
      // },
      'flg': {
        '$in': [
          '0x02', '0x12'
        ]
      }
    }
  },
  // {
  //   '$group': {
  //     '_id': {
  //       'da': '$da',
  //       'loc': '$dl'
  //     },
  //     'count': {
  //       '$sum': 1
  //     },
  //     'pkt': {
  //       '$sum': {
  //         '$switch': {
  //           'branches': [
  //             {
  //               'case': {
  //                 '$eq': [
  //                   '$flg', '0x02'
  //                 ]
  //               },
  //               'then': '$pkt'
  //             }
  //           ],
  //           'default': 0
  //         }
  //       }
  //     },
  //     'ips': {
  //       '$addToSet': '$sa'
  //     },
  //     'ack': {
  //       '$sum': {
  //         '$switch': {
  //           'branches': [
  //             {
  //               'case': {
  //                 '$eq': [
  //                   '$flg', '0x12'
  //                 ]
  //               },
  //               'then': 1
  //             }
  //           ],
  //           'default': 0
  //         }
  //       }
  //     },
  //     'syn': {
  //       '$sum': {
  //         '$switch': {
  //           'branches': [
  //             {
  //               'case': {
  //                 '$eq': [
  //                   '$flg', '0x02'
  //                 ]
  //               },
  //               'then': 1
  //             }
  //           ],
  //           'default': 0
  //         }
  //       }
  //     }
  //   }
  // },
  {
    '$bucket': {
      'groupBy': '$te',
      'boundaries': [
        new Date('2020-03-14T00:00:00+00:00'),
        new Date('2020-03-14T01:00:00+00:00'),
        new Date('2020-03-14T02:00:00+00:00'),
        new Date('2020-03-14T03:00:00+00:00'),
        new Date('2020-03-14T04:00:00+00:00'),
        new Date('2020-03-14T05:00:00+00:00'),
        new Date('2020-03-14T06:00:00+00:00'),
        new Date('2020-03-14T07:00:00+00:00'),
        new Date('2020-03-14T08:00:00+00:00'),
        new Date('2020-03-14T09:00:00+00:00'),
        new Date('2020-03-14T10:00:00+00:00'),
        new Date('2020-03-14T11:00:00+00:00'),
        new Date('2020-03-14T12:00:00+00:00'),
        new Date('2020-03-14T13:00:00+00:00'),
        new Date('2020-03-14T14:00:00+00:00'),
        new Date('2020-03-14T15:00:00+00:00'),
        new Date('2020-03-14T16:00:00+00:00'),
        new Date('2020-03-14T17:00:00+00:00'),
        new Date('2020-03-14T18:00:00+00:00'),
        new Date('2020-03-14T19:00:00+00:00'),
        new Date('2020-03-14T20:00:00+00:00'),
        new Date('2020-03-14T21:00:00+00:00'),
        new Date('2020-03-14T22:00:00+00:00'),
        new Date('2020-03-14T23:00:00+00:00'),
        new Date('2020-03-15T00:00:00+00:00'),
        new Date('2020-03-15T01:00:00+00:00'),
        new Date('2020-03-15T02:00:00+00:00'),
        new Date('2020-03-15T03:00:00+00:00'),
        new Date('2020-03-15T04:00:00+00:00'),
        new Date('2020-03-15T05:00:00+00:00'),
        new Date('2020-03-15T06:00:00+00:00'),
        new Date('2020-03-15T07:00:00+00:00'),
        new Date('2020-03-15T08:00:00+00:00'),
        new Date('2020-03-15T09:00:00+00:00'),
        new Date('2020-03-15T10:00:00+00:00'),
        new Date('2020-03-15T11:00:00+00:00'),
        new Date('2020-03-15T12:00:00+00:00'),
        new Date('2020-03-15T13:00:00+00:00'),
        new Date('2020-03-15T14:00:00+00:00'),
        new Date('2020-03-15T15:00:00+00:00'),
        new Date('2020-03-15T16:00:00+00:00'),
        new Date('2020-03-15T17:00:00+00:00'),
        new Date('2020-03-15T18:00:00+00:00'),
        new Date('2020-03-15T19:00:00+00:00'),
        new Date('2020-03-15T20:00:00+00:00'),
        new Date('2020-03-15T21:00:00+00:00'),
        new Date('2020-03-15T22:00:00+00:00'),
        new Date('2020-03-15T23:00:00+00:00'),
        new Date('2020-03-16T00:00:00+00:00')
      ],
      'default': 'rest',
      'output': {
        'count':  { '$sum': 1 },
        'pkt': {
          '$sum': {
            '$switch': {
              'branches': [
                {
                  'case': {
                    '$eq': [
                      '$flg', '0x02'
                    ]
                  },
                  'then': '$pkt'
                }
              ],
              'default': 0
            }
          }
        },
        'ips': {
          '$addToSet': '$sa'
        },
        'ack': {
          '$sum': {
            '$switch': {
              'branches': [
                {
                  'case': {
                    '$eq': [
                      '$flg', '0x12'
                    ]
                  },
                  'then': 1
                }
              ],
              'default': 0
            }
          }
        },
        'syn': {
          '$sum': {
            '$switch': {
              'branches': [
                {
                  'case': {
                    '$eq': [
                      '$flg', '0x02'
                    ]
                  },
                  'then': 1
                }
              ],
              'default': 0
            }
          }
        }
      }
    }
  },
  {
    '$match': {
      'pkt': {
        '$gt': 1
      }
    }
  },
  {
    '$addFields': {
      'ips_length': {
        '$size': '$ips'
      }
    }
  },
  {
    '$sort': {
      'ips_length': -1
    }
  }
];

MongoClient.connect(
  'mongodb://10.147.20.92:27017',
  { useNewUrlParser: true, useUnifiedTopology: true },
  async function (connectErr, client) {
    const coll = client.db('vd').collection('data_all');
    const cursor = coll.aggregate(agg)
    const data = (await cursor.toArray()).map(({ _id, ...rest }) => ({ ...rest, te: _id }))
    console.log(data);
    const insertCollection = client.db('vd').collection('data_attacks')
    await insertCollection.insertMany(data);
    client.close();
  });