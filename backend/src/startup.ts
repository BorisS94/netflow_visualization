import 'reflect-metadata';
import { get as getConfig } from 'config';
import { NetFlowCoreConfig } from './core/infrastructure';
import NetFlowCoreBootstrapper from './core/bootstrapper';
import NETFLOW_MODULE_CONFIG from './netflow-module/ioc-config';

export class Startup {
    private readonly netFlowCoreBootstrapper = new NetFlowCoreBootstrapper();

    public async start() {
        const CORE_CONFIG = getConfig<NetFlowCoreConfig>('core');
        await this.netFlowCoreBootstrapper.bootstrap(CORE_CONFIG, NETFLOW_MODULE_CONFIG)
    }
}