import { Startup } from './startup';

async function runApplication() {
    const startup = new Startup();
    try {
        await startup.start();
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}

runApplication();