import {
  connection,
  SchemaDefinition,
  model,
  connect,
  Schema,
  Model,
} from 'mongoose';
import { BootstrapService } from '../core/common';

const GIGABYTE_DIVISION = 1000000000;

const schemaDef: SchemaDefinition = {
  dl: [Number],
  sl: [Number],
  sc: String,
  dc: String,
  flg: String,
  ts: Date,
  te: Date,
  in: Number,
  out: Number,
  bpp: Number,
  bps: Number,
  pkt: Number,
  byt: Number,
  sa: String,
  da: String,
  ra: String,
  pr: String,
  sp: Number,
  dp: Number,
  it: String,
  ic: String,
};
export class MongoService implements BootstrapService {
  private dataModel: Model<any>;
  private maintenanceModel: Model<any>
  private fakeClientModel: Model<any>
  private clientModel: Model<any>
  private providersModel: Model<any>

  async bootstrapService() {
    await connect('mongodb://10.147.20.92:27017/vd', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      connectTimeoutMS: 30 * 60 * 1000,
      socketTimeoutMS: 1000 * 60 * 30
    });
    connection.on('error', (err) => console.error(err));
    const schema = new Schema(schemaDef);
    this.dataModel = model('data', schema, 'data_all', false);
    this.fakeClientModel = model('fake_client', schema, 'data', false);
    this.clientModel = model('provider', schema, 'data_providers', false)
    const maintenanceSchema = new Schema({ byt: Number, bpp: Number, bps: Number, pkt: Number, te: Date })
    this.maintenanceModel = model('maintenance', maintenanceSchema, 'data_maintain', false)
    this.providersModel = model('client', schema, 'data_clients', false)
  }

  async aggregate(start: string, end: string) {
    const result = await this.clientModel
      .aggregate()
      .match({
        in: { $lt: 1024 },
        te: { $gte: new Date(start), $lt: new Date(end) },
      })
      .group({
        _id: {
          sl: '$sl', sc: '$sc'
        },
        byt: { $sum: '$byt' },
        pkt: { $sum: '$pkt' },
      }); // .exec((err, res) => {console.log(res.map(r => ({...r, _id: toString(r._id)})))})
    // return result
    return result;
  }

  async clients(start: string, end: string) {
    const data = await this.fakeClientModel
      .aggregate()
      .match({
        te: { $gte: new Date(start), $lt: new Date(end) },
      }).group({
        _id: "$sl",
        clients: {
          "$addToSet": "$dl"
        }
      }).addFields({ count: { $size: "$clients" } }).sort({ count: -1 })
    return data.map(({ _id: { coordinates }, clients }) => ({ sc: coordinates, clients: clients.map(c => c.coordinates).slice(0, 20) }))
  }

  async realeClients(start: string, end: string) {
    const data = await this.clientModel
      .aggregate()
      .match({
        te: { $gte: new Date(start), $lt: new Date(end) },
      }).group({
        _id: "$sl",
        clients: {
          "$addToSet": "$dl"
        }
      }).addFields({ count: { $size: "$clients" } }).sort({ count: -1 })
    return data
  }

  getMaintenance(start: string, end: string) {
    return this.maintenanceModel.find({ te: { $gte: new Date(start), $lt: new Date(end) } })
  }

  getBarChartData() {
    return this.dataModel
      .aggregate()
      .group({ _id: '$sc', byt: { $sum: '$byt' } })
      .project({ byt: { $divide: ['$byt', GIGABYTE_DIVISION] } });
  }

  getPieChartData() {
    return this.dataModel
      .aggregate()
      .group({ _id: '$sc', pkt: { $sum: '$pkt' } });
  }

  getPieDataRange(start: string, end: string) {
    return this.clientModel
      .aggregate()
      .match({ te: { $gte: new Date(start), $lt: new Date(end) } })
      .group({ _id: '$dc', pkt: { $sum: '$pkt' } });
  }

  getBarDataRange(start: string, end: string) {
    return this.clientModel
      .aggregate()
      .match({ te: { $gte: new Date(start), $lt: new Date(end) } })
      .group({ _id: '$dc', byt: { $sum: '$byt' } })
      .project({ byt: { $divide: ['$byt', GIGABYTE_DIVISION] } });
  }

  getSynFloodAttack() {
    return (this.dataModel.aggregate().allowDiskUse(true).match({
      'te': {
        '$gte': new Date('2020-03-14T00:00:00+00:00'),
        '$lt': new Date('2020-03-14T01:00:00+00:00')
      },
      'flg': {
        '$in': [
          '0x02', '0x12'
        ]
      }
    }) as any).group({
      '_id': {
        'da': '$da',
        'loc': '$dl'
      },
      'count': {
        '$sum': 1
      },
      'pkt': {
        '$sum': {
          '$switch': {
            'branches': [
              {
                'case': {
                  '$eq': [
                    '$flg', '0x02'
                  ]
                },
                'then': '$pkt'
              }
            ],
            'default': 0
          }
        }
      },
      'ips': {
        '$addToSet': '$sa'
      },
      'ack': {
        '$sum': {
          '$switch': {
            'branches': [
              {
                'case': {
                  '$eq': [
                    '$flg', '0x12'
                  ]
                },
                'then': 1
              }
            ],
            'default': 0
          }
        }
      },
      'syn': {
        '$sum': {
          '$switch': {
            'branches': [
              {
                'case': {
                  '$eq': [
                    '$flg', '0x02'
                  ]
                },
                'then': 1
              }
            ],
            'default': 0
          }
        }
      }
    }, { $maxTimeMS: 30 * 60 * 1000 })
  }

  async getSunbrust(start: string, end: string) {
    const data = await this.clientModel
      .aggregate()
      .match({ te: { $gte: new Date(start), $lt: new Date(end) } })
      .group({ _id: { dc: '$dc', da: '$da' }, byt: { $sum: '$byt' } })
      .sort({ byt: -1 })
    const result = []
    for (const { _id: { da, dc }, byt } of data) {
      const f = result.find(r => r.name === dc)
      if (f && f.children.length < 10) {
        const c = f.children.find(ch => ch.name === da)
        if (c) {
          c.size += byt
        } else {
          f.children.push({ name: da, size: byt })
        }
      } else if (!!!f) {
        result.push({ name: dc, children: [{ name: da, size: byt }] })
      }
    }
    return { name: 'world', children: result };
  }

  async bySubnet() {
    return this.dataModel.aggregate().addFields({
      result: {
        $regexMatch: {
          input: "$sa",
          regex: /^109\.74\.14\d/gm
        }
      }
    }).match({ result: true }).limit(1000)
  }

  // getClientDetail(coords) {
  //   return this.clientModel.
  // }

  async getClients(start: string, end: string, ips: string[]) {
    const data = await this.clientModel.aggregate().match({
      te: {
        $gte: new Date(start),
        $lt: new Date(end)
      },
      sa: { $in: ips }
    }).group(
      {
        _id: "$dl",
        arr: {
          $push: {da: '$da', byt: '$byt'}
        }
      }
    )
    const response = []
    for (const { _id, arr } of data) {
      const result = {}
      let ovrByt = 0
      for(const { da, byt} of arr) {
        if(!!result[da]) {
          result[da] += byt
        }
        else {
          result[da] = byt
        }
        ovrByt += byt
      }
      response.push({dl: _id, clients: result, byt: ovrByt})
    }
    return response.sort((a, b) => b.byt - a.byt).slice(0, 100)
  }

  testDetail(start: string, end: string, sl: [number, number], sc: string) {
    return this.clientModel.aggregate().match({
      $or: [
        { sl },
        { dl: sl }
      ],
      te: {
        $gte: new Date(start),
        $lt: new Date(end)
      }
    }).group({
      _id: "$dc",
      ips: {
        $addToSet: {
          $cond: {
            if: {
              $eq: ["$dl", sl]
            },
            then: "$sa",
            else: null
          }
        }
      }
    }).match({ _id: sc })
  }

  getProviders(start, amount) {
    console.log(amount)
    const data = require(__dirname + '/providers_' + (start < 10 ? '0' + start : start) + '.json')
    return {...data, children: data.children.map(d => ({
      name: d.name,
      children: d.children.filter(c => c.sum >= amount)
    }))
    }
  }
}
