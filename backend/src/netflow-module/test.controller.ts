import { RestController, Get, Post } from "../core/communication";
import { Logger } from "../core/infrastructure";
import { Request } from 'express';
import { MockService } from './mock.service';
import { Inject } from '../core/infrastructure/ioc';
import { MongoService } from './mongo.service';
import { ElasticService } from './elastic.service';
import moment from 'moment';

@RestController('test')
export class TestController {
    @Inject mock: MockService
    @Inject mongo: MongoService
    @Inject private readonly elasticService: ElasticService

    constructor() {
        Logger.info('TestController');
    }

    @Get({
        path: '',
    })
    public async test(req: Request) {
        return 'Hello friend!';
    }

    @Get({
        path: 'netflow',
    })
    public async testNetflow(req: Request) {
        return 'Hello NetFlow!'
    }

    @Get({
        path: 'bar-data'
    })
    public barChartData() {
        return this.mongo.getBarChartData()
    }

    @Get({
        path: 'pie-data'
    })
    public pieChartData() {
        return this.mongo.getPieChartData()
    }

    @Get({
        path: 'donut-data'
    })
    public donutChartData() {
        return this.mock.getDonutChartData()
    }

    @Get({
        path: 'providers'
    })
    public providers() {
        return this.mock.getProviders()
    }

    @Post({
        path: 'mongo-test'
    })
    public mongoTest(req: Request) {
        const { start, end } = req.body;
        return this.mongo.aggregate(start, end)
    }

    @Post({
        path: 'maintain'
    })
    public getMaintenance(req: Request) {
        const { start, end } = req.body;
        return this.mongo.getMaintenance(start, end)
    }

    @Post({
        path: 'pie-data'
    })
    public getPieDataRange(req: Request) {
        const { start, end } = req.body;
        return this.mongo.getPieDataRange(start, end)
    }

    @Post({
        path: 'bar-data'
    })
    public getBarDataRange(req: Request) {
        const { start, end } = req.body;
        return this.mongo.getBarDataRange(start, end)
    }

    @Get({
        path: 'syn-flood/:day'
    })
    public getSynFloodAttack(req: Request) {
        let { day } = req.params
        day = moment(day).format('DD')
        return this.elasticService.getSynFlood(day)
    }

    @Get({
        path: 'rst/:day'
    })
    public getRstAttack(req: Request) {
        let { day } = req.params
        day = moment(day).format('DD')
        return this.elasticService.getRstAttack(day)
    }

    @Get({
        path: 'udp-flood/:day'
    })
    public getUdpFlood(req: Request) {
        let { day } = req.params
        day = moment(day).format('DD')
        return this.elasticService.getUdpAttacks(day)
    }

    @Post({
        path: 'clients'
    })
    public clients(req: Request) {
        const { start, end, ips } = req.body;
        return this.mongo.getClients(start, end, ips)
    }

    @Post({
        path: 'real-clients'
    })
    public realCleints(req: Request) {
        const { start, end } = req.body;
        return this.mongo.realeClients(start, end)
    }

    @Post({
        path: 'sunbrust'
    })
    public sunbrust(req: Request) {
        const { start, end } = req.body;
        return this.mongo.getSunbrust(start, end)
    }

    @Get({
        path: 'subnet'
    })
    public subnet() {
        return this.mongo.bySubnet()
    }

    @Post({
        path: 'detail'
    })
    public getDetail(req: Request) {
        const { start, end, sl, sc } = req.body;
        return this.mongo.testDetail(start, end, sl, sc)
    }

    @Post({
        path: 'providers'
    })
    public getProviders(req: Request) {
        const { start, amount } = req.body;
        return this.mongo.getProviders(start, amount)
    }
}