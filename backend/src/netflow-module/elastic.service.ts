import { Client } from '@elastic/elasticsearch';
import { BootstrapService } from '../core/common';
import moment from 'moment';

export class ElasticService implements BootstrapService {
  private elasticClient: Client

  async bootstrapService() {
    this.elasticClient = new Client({
      node: 'http://10.147.20.92:9200'
    });
  }

  async getSynFlood(day: string) {
    const { body: { aggregations: { my_buckets: { buckets } } } } = await this.elasticClient.search({
      index: `data_${day}_03`,
      size: 0,
      body: {
        size: 0,
        query: {
          bool: {
            must: {
              terms: { flg: ['0x02', '0x12'] }
            },
            filter: [
              {
                range: {
                  da: {
                    gte: '109.74.144.0',
                    lte: '109.74.145.255'
                  }
                }
              }
            ]
          }
        },
        aggs: {
          my_buckets: {
            composite: {
              size: 10000,
              sources: [
                { date: { date_histogram: { field: 'te', calendar_interval: '1h' } } },
                { addr: { terms: { field: 'da' } } }
              ]
            },
            aggregations: {
              pkt: {
                sum: { field: 'pkt' }
              },
              syn: {
                sum: {
                  script: {
                    lang: 'painless',
                    source: 'doc.flg.value == \"0x02\" ? 1 : 0'
                  }
                }
              },
              ack: {
                sum: {
                  script: {
                    lang: 'painless',
                    source: 'doc.flg.value == \"0x12\" ? 1 : 0'
                  }
                }
              },
              diff: {
                bucket_script: {
                  buckets_path: {
                    ack: 'ack',
                    syn: 'syn'
                  },
                  script: 'params.syn - params.ack'
                }
              },
              pkt_sort: {
                bucket_sort: {
                  sort: [
                    { diff: { order: 'desc' } }
                  ],
                  size: 1000
                }
              }
            }
          }
        }
      }
    })
    const result = {}
    for (const b of buckets) {
      const date = moment(b.key.date).format('H')
      if (!result[date]) {
        result[date] = b.diff.value
      } else {
        result[date] += b.diff.value
      }
    }
    return result
  }

  async getRstAttack(day: string) {
    const { body: { aggregations: { my_buckets: { buckets } } } } = await this.elasticClient.search({
      index: `data_${day}_03`,
      size: 0,
      body: {
        query: {
          bool: {
            must: {
              term: { flg: '0x04' }
            },
            filter: [
              {
                range: {
                  da: {
                    gte: '109.74.144.0',
                    lte: '109.74.145.255'
                  }
                }
              }
            ]
          }
        },
        aggs: {
          my_buckets: {
            composite: {
              size: 10000,
              sources: [
                { date: { date_histogram: { field: 'te', calendar_interval: '1h' } } },
                { addr: { terms: { field: 'da' } } }
              ]
            },
            aggregations: {
              pkt: {
                sum: { field: 'pkt' }
              },
              pkt_sort: {
                bucket_sort: {
                  sort: [
                    { pkt: { order: 'desc' } }
                  ],
                  size: 1000
                }
              }
            }
          }
        }
      }
    })
    const result = {}
    for (const b of buckets) {
      const date = moment(b.key.date).format('H')
      if (!result[date]) {
        result[date] = b.pkt.value
      } else {
        result[date] += b.pkt.value
      }
    }
    return result
  }

  async getUdpAttacks(day: string) {
    const { body: { aggregations: { my_buckets: { buckets } } } } = await this.elasticClient.search({
      index: `data_${day}_03`,
      size: 0,
      body: {
        query: {
          bool: {
            must: {
              term: { pr: 'UDP' }
            },
            filter: [
              {
                range: {
                  da: {
                    gte: "109.74.144.0",
                    lte: "109.74.145.255"
                  }
                }
              }
            ]
          }
        },
        aggs: {
          my_buckets: {
            composite: {
              size: 10000,
              sources: [
                { date: { date_histogram: { field: 'te', calendar_interval: '1h' } } },
                { addr: { terms: { field: 'da' } } }
              ]
            },
            aggregations: {
              pkt: {
                sum: { field: 'pkt' }
              },
              ports: {
                cardinality: {
                  field: 'in',
                  precision_threshold: 10000
                }
              },
              pkt_sort: {
                bucket_sort: {
                  sort: [


                    { pkt: { order: 'desc' } }
                  ],
                  size: 100
                }
              }
            }
          }
        }
      }
    })
    const result = {}
    for (const b of buckets) {
      const date = moment(b.key.date).format('H')
      if (!result[date]) {
        result[date] = b.pkt.value
      } else {
        result[date] += b.pkt.value
      }
    }
    return result
  }
}
