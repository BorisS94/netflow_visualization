import { readFile } from 'fs';
import neatCsv from 'neat-csv';

export class MockService {
  gerBarChartData() {
    const d = require('C:\\School\\vd\\data_json\\nf_data_inet_15.json');
    const result = {};
    for (const { sc, byt } of d) {
      if (sc && byt) {
        result[sc] = result[sc]
          ? result[sc] + byt / 1000000000
          : byt / 1000000000;
      }
    }
    return result;
    // return new Promise((resolve, reject) => {
    //   readFile(
    //     '/home/jan/school/vd/dataset/nf_data_inet_14.csv',
    //     async (err, data) => {
    //       if (err) {
    //         console.error(err);
    //         reject(err);
    //       }
    //       const resolved = await neatCsv(data);
    //       console.log(resolved[0]);
    //       let result = resolved
    //         .map(
    //           ({ sa_country, byt }: { sa_country: string; byt: string }) => ({
    //             source: sa_country,
    //             amount: +byt / 1000000000,
    //           })
    //         )
    //         .filter(({ source, amount }) => source && amount)
    //         .reduce(
    //           (prev, { source, amount }) => ({
    //             ...prev,
    //             [source]: prev[source] ? prev[source] + amount : amount,
    //           }),
    //           {}
    //         );

    //       result = Object.keys(result).map((key) => ({
    //         source: key,
    //         amount: result[key],
    //       }));
    //       resolve(result);
    //     }
    //   );
    // });
  }

  getPieChartData() {
    const d = require('C:\\School\\vd\\data_json\\nf_data_inet_15.json');
    const result = {};
    for (const { sc, pkt } of d) {
      if (sc && pkt) {
        result[sc] = result[sc] ? result[sc] + pkt : pkt;
      }
    }
    return result;
    // return new Promise((resolve, reject) => {
    //   readFile(
    //     '/home/jan/school/vd/dataset/nf_data_inet_14.csv',
    //     async (err, data) => {
    //       if (err) {
    //         console.error(err);
    //         reject(err);
    //       }
    //       const resolved = await neatCsv(data);
    //       console.log(resolved[0]);
    //       let result = resolved
    //         .map(
    //           ({ sa_country, pkt }: { sa_country: string; pkt: string }) => ({
    //             source: sa_country,
    //             amount: +pkt,
    //           })
    //         )
    //         .filter(({ source, amount }) => source && amount)
    //         .reduce(
    //           (prev, { source, amount }) => ({
    //             ...prev,
    //             [source]: prev[source] ? prev[source] + amount : amount,
    //           }),
    //           {}
    //         );

    //       result = Object.keys(result).map((key) => ({
    //         source: key,
    //         amount: result[key],
    //       }));
    //       resolve(result);
    //     }
    //   );
    // });
  }

  getDonutChartData() {
    return new Promise((resolve, reject) => {
      readFile(
        '/home/jan/school/vd/dataset/nf_data_inet_14.csv',
        async (err, data) => {
          if (err) {
            console.error(err);
            reject(err);
          }
          const resolved = await neatCsv(data);
          console.log(resolved[0]);
          let result = resolved
            .map(({ pr }: { pr: string }) => pr)
            .filter((pr) => Number.isNaN(Number.parseInt(pr, null)))
            .reduce((prev, pr) => ({ ...prev, [pr]: (prev[pr] ?? 0) + 1 }), {});

          result = Object.keys(result).map((key) => ({
            protocol: key,
            amount: result[key],
          }));
          resolve(result);
        }
      );
    });
  }

  getProviders() {
    const d = require('C:\\School\\vd\\data_json\\nf_data_inet_15.json');
    const result = {};
    for (const { da, dl } of d) {
      if (result[da]) {
        result[da].c += 1;
      } else {
        result[da] = {
          c: 1,
          dl,
        };
      }
    }
    return result;
    // return new Promise((resolve, reject) => {
    //   readFile(
    //     'C:\\School\\vd\\data_json\\nf_data_inet_15.json',
    //     { encoding: 'utf-8' },
    //     async (err, data) => {
    //       if (err) {
    //         console.error(err);
    //         reject(err);
    //       }
    //       const result = {};
    //       const d = JSON.parse(data);
    //       for (const value of d) {
    //         const { da, dl } = value;
    //         if (result[da]) {
    //           result[da].c += 1;
    //         } else {
    //           result[da] = {
    //             c: 1,
    //             dl,
    //           };
    //         }
    //       }
    //       resolve(result);
    //     }
    //   );
    // });
  }
}
