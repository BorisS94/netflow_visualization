import { TestController } from "./test.controller";
import { Scope } from "typescript-ioc";
import { ProvidableConfiguration } from "../core/infrastructure/ioc";
import { MockService } from './mock.service';
import { MongoService } from './mongo.service';
import { ElasticService } from './elastic.service';

export default [
  { bind: TestController, scope: Scope.Singleton },
  { bind: MockService, scope: Scope.Local },
  { bind: MongoService, scope: Scope.Singleton, isBootstrappable: true },
  { bind: ElasticService, scope: Scope.Singleton, isBootstrappable: true }
] as ProvidableConfiguration[];
