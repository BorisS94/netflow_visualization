export interface BootstrapService {
    bootstrapService(): Promise<void>;
}
