import { RedisClient } from 'redis';
import { Logger } from '../../infrastructure';
import { BootstrapService } from '../../common/bootstrap.service';
import { NetFlowRedisConfig } from './redis.ioc-config';

enum RedisState {
  OK = 'OK',
  ERROR = 'ERROR',
}

/**
 * Wraps redis operations and connection handling
 */
export class RedisService implements BootstrapService {
  private redis: RedisClient;
  private state: RedisState;

  constructor(private readonly config: NetFlowRedisConfig) { }

  public async bootstrapService() {
    return this.initialize();
  }

  private async initialize() {
    Logger.info(`Initializing connection to ${this.config.host}:${this.config.port}`);
    this.redis = new RedisClient(this.config);
    this.handleEvents();
  }

  public async setKey(key: string, value: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.set(key, value, (error, response) => {
            error ? reject(null) : resolve(response);
          })
        : reject(null);
    });
  }

  public async setKeyExpire(key: string, value, expiresIn: number) {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.set(key, value, 'EX', expiresIn, (error, response) => {
            error ? reject(null) : resolve(response);
          })
        : reject(null);
    });
  }

  public async getKey(key: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.get(key, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  public async delKey(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.del(key, (error, response) => {
            error ? reject(null) : resolve(response);
          })
        : reject(null);
    });
  }

  public async hsetKey(key: string, field: string, value: string): Promise<number | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.hset(key, field, value, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  public async hgetKey(key: string, field: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.hget(key, field, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  public async hmSet(key: string, fieldValuePairs: string[]): Promise<string | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.hmset(key, fieldValuePairs, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  public async hScan(key: string, pattern: string) {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.hscan(key, '0', 'MATCH', pattern, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  public async hdel(key: string, fields: string[]): Promise<number | null> {
    return new Promise((resolve, reject) => {
      this.stateOk()
        ? this.redis.hdel(key, fields, (error, response) => {
            error ? reject(error) : resolve(response);
          })
        : reject(null);
    });
  }

  private handleEvents() {
    this.redis.on('error', error => {
      Logger.error(`Redis error: ${error}`);
    });
    this.redis.on('ready', () => {
      Logger.info(`Redis ready`);
      this.state = RedisState.OK;
    });
    this.redis.on('end', () => {
      Logger.info(`Redis connection end`);
      this.state = RedisState.ERROR;
    });
  }

  private stateOk(): boolean {
    return this.state === RedisState.OK;
  }
}
