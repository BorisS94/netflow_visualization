import { ObjectFactory, Scope } from "typescript-ioc";
import { RedisService } from ".";
import { ProvidableConfiguration } from "../../infrastructure/ioc";

export interface NetFlowRedisConfig {
    host: string;
    port: number;
}

const RedisProvider = (config: NetFlowRedisConfig): ObjectFactory => () => new RedisService(config);

export default (config: NetFlowRedisConfig) =>
  [{ bind: RedisService, scope: Scope.Singleton, factory: RedisProvider(config), isBootstrappable: true }] as ProvidableConfiguration[];