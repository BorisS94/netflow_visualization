import { BootstrapService } from "../../common";
import { NetFlowInfluxConfig } from "./influx.ioc-config";
import { Logger } from "../../infrastructure";
import { InfluxDB } from 'influx';

export class InfluxConnector implements BootstrapService{

    private influx: InfluxDB;

    constructor(private readonly config: NetFlowInfluxConfig) {}

    bootstrapService(): Promise<void> {
        return this.initialize();
    }

    private async initialize() {
        Logger.info(`Initializing connection to ${this.config.host}`);
        this.influx = new InfluxDB({
            host: this.config.host,
            database: this.config.database
        });
        Logger.info(`Connected`);
    }

}