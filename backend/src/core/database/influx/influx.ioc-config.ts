import { ObjectFactory, Scope } from "typescript-ioc";
import { InfluxConnector } from "./influx.connector";
import { ProvidableConfiguration } from "../../infrastructure/ioc";

export interface NetFlowInfluxConfig {
    host: string;
    database: string;
}

const InfluxProvider = (config: NetFlowInfluxConfig): ObjectFactory => () => new InfluxConnector(config);

export default (config: NetFlowInfluxConfig) =>
  [{ bind: InfluxConnector, scope: Scope.Singleton, factory: InfluxProvider(config), isBootstrappable: true }] as ProvidableConfiguration[];