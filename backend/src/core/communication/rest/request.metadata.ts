import { OK } from 'http-status-codes';
import { RouteDefinition } from './route';

export interface MethodParams {
  path: string;
  onSuccess?: number;
}

export const Get = (methodParams: MethodParams): MethodDecorator => (target: object, propertyKey: string | symbol): void => {
  initiateRoutesIfEmpty(target);
  const currentRoutes = getCurrentRoutes(target);
  pushNewRoute(currentRoutes, 'get', propertyKey, methodParams, target);
};

export const Post = (methodParams: MethodParams): MethodDecorator => (target: object, propertyKey: string | symbol): void => {
  initiateRoutesIfEmpty(target);
  const currentRoutes = getCurrentRoutes(target);
  pushNewRoute(currentRoutes, 'post', propertyKey, methodParams, target);
};

export const Put = (methodParams: MethodParams): MethodDecorator => (target: object, propertyKey: string | symbol): void => {
  initiateRoutesIfEmpty(target);
  const currentRoutes = getCurrentRoutes(target);
  pushNewRoute(currentRoutes, 'put', propertyKey, methodParams, target);
};

export const Delete = (methodParams: MethodParams): MethodDecorator => (target: object, propertyKey: string | symbol): void => {
  initiateRoutesIfEmpty(target);
  const currentRoutes = getCurrentRoutes(target);
  pushNewRoute(currentRoutes, 'delete', propertyKey, methodParams, target);
};

function initiateRoutesIfEmpty(target) {
  if (!Reflect.hasMetadata('routes', target.constructor)) {
    Reflect.defineMetadata('routes', [], target.constructor);
  }
}

function getCurrentRoutes(target) {
  return Reflect.getMetadata('routes', target.constructor) as RouteDefinition[];
}

function pushNewRoute(
  currentRoutes: any,
  requestMethod: string,
  methodName: string | symbol,
  methodParams: MethodParams,
  targetController: any
) {
  const { path, onSuccess } = methodParams;
  currentRoutes.push({
    requestMethod,
    path: '/' + path,
    methodName,
    onSuccess: onSuccess ?? 200
  });
  Reflect.defineMetadata('routes', currentRoutes, targetController.constructor);
}
