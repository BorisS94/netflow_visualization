import { ExpressService } from './express.service';
import { RouteDefinition } from './route';
import { Request, Response } from 'express';
import { NetFlowCoreServiceProviders, NetFlowCoreProvider } from '../../infrastructure/ioc';
import { Container } from 'typescript-ioc';
import { Logger } from '../../infrastructure';

export const RestController = (prefix: string = ''): ClassDecorator => {
  return (target: object) => {
    NetFlowCoreProvider.setProvider(NetFlowCoreServiceProviders.REST_CONTROLLER, target);
    Reflect.defineMetadata('prefix', '/' + prefix, target);

    if (!Reflect.hasMetadata('routes', target)) {
      Reflect.defineMetadata('routes', [], target);
    }
  };
};

/**
 * Apply rest controller metadata of service configurator
 * @param controllers - Array of REST API controller classes
 * @param service - Express Service instance
 */
export const applyRestControllerMetadata = () => {
  const controllers = NetFlowCoreProvider.getProviders(NetFlowCoreServiceProviders.REST_CONTROLLER);
  const app = Container.get(ExpressService).getExpressApp();

  controllers.forEach((controller: any) => {
    const routePrefix = Reflect.getMetadata('prefix', controller);

    Logger.info(`Loading routes for ${routePrefix}`);
    const loadedRoutes = loadRoutes(controller, routePrefix);

    const controllerInstance = Container.get(controller);
    loadedRoutes.forEach(loadedRoute => {
      Logger.info(
        `Route: fullpath: ${loadedRoute.fullPath}, requestMethod: ${loadedRoute.route.requestMethod}, methodName: ${JSON.stringify(loadedRoute.route.methodName)}, onSuccess: ${loadedRoute.route.onSuccess}`
      );
      serveRouteRequests(loadedRoute.route, loadedRoute.fullPath, app, controllerInstance);
    });
  });
};

function loadRoutes(controller, routePrefix): { route: RouteDefinition; fullPath: string}[] {
  const routes: RouteDefinition[] = Reflect.getMetadata('routes', controller);
  return routes.map(route => {
    const fullPath = routePrefix + route.path;
    return { route, fullPath };
  });
}

function serveRouteRequests(route: RouteDefinition, fullPath: string, app, controllerInstance) {
  app[route.requestMethod](fullPath, async (request: Request, response: Response) => {
    try {
      const result = await controllerInstance[route.methodName](request, response);
      response.status(route.onSuccess).json(result);
    } catch (err) {
      if (err?.statusCode) {
        response.statusMessage = err.statusText;
        response.status(err.statusCode).json(err.statusBody);
      } else {
        console.error('Internal server error:', err);
        response.sendStatus(500);
      }
    }
  });
}
