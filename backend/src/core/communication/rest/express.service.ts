import express, {Express} from 'express';
import { json as jsonBodyParser } from 'body-parser';
import { NetFlowExpressConfig } from './express.ioc-config';
import { applyRestControllerMetadata } from './controller.metadata';
import { Inject } from 'typescript-ioc';
import { BootstrapService } from '../../common/bootstrap.service';
import { Logger } from '../../infrastructure';

export class ExpressService implements BootstrapService {
  private app: Express;

  constructor(private readonly config: NetFlowExpressConfig) {}

  public async bootstrapService() {
    return this.initialize().then(() => applyRestControllerMetadata());
  }

  public async initialize() {
    Logger.info(`Initializing`);
    try {
      this.app = express();
      this.app.use(jsonBodyParser());
      this.app.listen(this.config.port);
      Logger.info(`Listening on port ${this.config.port}`);
    } catch (error) {
      Logger.error(`Failed to listen on REST port ${this.config.port}`);
      throw new Error(error);
    }
  }

  public getExpressApp(): Express {
    return this.app;
  }
}
