import { ObjectFactory, Scope } from 'typescript-ioc';
import { ProvidableConfiguration } from '../../infrastructure/ioc';
import { ExpressService } from './express.service';

export interface NetFlowExpressConfig {
  port: number;
}

const ExpressProvider = (config: NetFlowExpressConfig): ObjectFactory => () => new ExpressService(config);

export default (config: NetFlowExpressConfig) =>
  [{ bind: ExpressService, scope: Scope.Singleton, factory: ExpressProvider(config), isBootstrappable: true }] as ProvidableConfiguration[];
