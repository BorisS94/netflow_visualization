import { NetFlowCoreConfig, IoCContainerConfigurator, ProvidableConfiguration } from './infrastructure/ioc';
import { Container } from 'typescript-ioc';
import { Logger, NetFlowLoggingConfig } from './infrastructure';

/**
 * Entry point to bootstrap and initialize all required dependencies
 */
export class NetFlowCoreBootstrapper {

  public async bootstrap(core: NetFlowCoreConfig, external: ProvidableConfiguration[] = []) {
    Logger.configureLogger(core.logging);
    Logger.info('Bootstrapping services...');

    IoCContainerConfigurator.configure(core, external);
    await this.initializeCoreConfiguration();
  }

  private async initializeCoreConfiguration() {
    const providables = IoCContainerConfigurator.getProvidableDependencies().map(({ bind }) => Container.get<typeof bind>(bind).bootstrapService());
    return Promise.all([...providables]);
  }

}

export default NetFlowCoreBootstrapper;
