import * as winston from 'winston';
import { get } from 'stack-trace';

export interface NetFlowLoggingConfig {
  level: string;
}

export class Logger {
  private static logger: winston.Logger;

  public static debug(msg: string) {
    return this.logger.debug(`${this.getCallerName()}${msg}`);
  }

  public static info(msg: string) {
    return this.logger.info(`${this.getCallerName()}${msg}`);
  }

  public static warn(msg: string) {
    return this.logger.debug(`${this.getCallerName()}${msg}`);
  }

  public static error(msg: string) {
    return this.logger.error(`${this.getCallerName()}${msg}`);
  }

  private static getTextFormat() {
    return winston.format.combine(
      winston.format.colorize(),
      winston.format.timestamp(),
      winston.format.printf(info => `${info.level} ${info.timestamp} ${info.message}`)
    );
  }

  private static getCallerName(): string {
    const stackFrames = get();
    const caller = stackFrames[2]?.getTypeName();
    return caller ? `[${caller}]: ` : '';
  }

  public static configureLogger(config: NetFlowLoggingConfig) {
    this.logger = winston.createLogger();
    this.logger.configure({
      level: config?.level ?? 'info',
      format: this.getTextFormat(),
      transports: [new winston.transports.Console()],
    });
  }
}
