import type { ContainerConfiguration } from 'typescript-ioc';

export interface ProvidableConfiguration extends ContainerConfiguration {
  isBootstrappable?: boolean;
}

export { Inject, Container, Scope, ObjectFactory } from 'typescript-ioc';