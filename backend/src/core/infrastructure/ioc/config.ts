import { NetFlowLoggingConfig } from "../logging/logger";
import { NetFlowExpressConfig } from "../../communication";
import { NetFlowInfluxConfig } from "../../database/influx";

export interface NetFlowCoreConfig {
    logging?: NetFlowLoggingConfig;
    express?: NetFlowExpressConfig;
    influx?: NetFlowInfluxConfig;
}

export type ServiceConfig = NetFlowLoggingConfig | NetFlowExpressConfig | NetFlowInfluxConfig