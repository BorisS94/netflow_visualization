export enum NetFlowCoreServiceProviders {
    REST_CONTROLLER = 'REST_CONTROLLER'
}

export interface ProviderEntry {
type: NetFlowCoreServiceProviders;
object: object;
}

/**
 * @internal
 * Providing mechanism for decorators
 */
export class NetFlowCoreProvider {
    private static providerStack = new Array<ProviderEntry>();

    /**
     * Gets providers
     * @param [type]
     * @returns
     */
    public static getProviders(type?: NetFlowCoreServiceProviders) {
        return type ? this.providerStack.filter(e => e.type === type).map(e => e.object) : this.providerStack.map(e => e.object);
    }

    /**
     * Sets provider
     * @param type
     * @param object
     */
    public static setProvider(type: NetFlowCoreServiceProviders, object: object) {
        this.providerStack.push({ type, object });
    }
}
