import type { ServiceConfig, NetFlowCoreConfig } from './config';
import { Logger } from '../logging/logger';
import { Container } from 'typescript-ioc';
import type { ProvidableConfiguration } from './ioc';
import { NetFlowExpressConfig } from '../../communication';
import { NetFlowRedisConfig } from '../../database/redis';
import { NetFlowInfluxConfig } from '../../database/influx/influx.ioc-config';
import EXPRESS_IOC_CONFIG from '../../communication/rest/express.ioc-config';
import REDIS_IOC_CONFIG from '../../database/redis/redis.ioc-config';
import INFLUX_IOC_CONFIG from '../../database/influx/influx.ioc-config';

export interface DIMapper<T = any> {
  [key: string]: (config: T) => ProvidableConfiguration[];
}

/**
 * Maps config dependencies to actual factory providers
 */
const BASE_DEPENDENCY_MAPPER: DIMapper<ServiceConfig> = {
  express: (config: NetFlowExpressConfig) => EXPRESS_IOC_CONFIG(config),
  redis: (config: NetFlowRedisConfig) => REDIS_IOC_CONFIG(config),
  influx: (config: NetFlowInfluxConfig) => INFLUX_IOC_CONFIG(config),
};

/**
 * IoC container configurator
 * Responsible for resolving dependencies
 */
export class IoCContainerConfigurator {
  /**
   * Gets depependencies
   * @returns all services included in DI
   */
  public static getDependencies() {
    return this.DEPENDENCIES;
  }

  /**
   * Gets providable dependencies
   * @returns bootstrapable services
   */
  public static getProvidableDependencies() {
    return this.DEPENDENCIES.filter(d => d.isBootstrappable);
  }

  /**
   * Deps of ioccontainer configurator
   */
  private static DEPENDENCIES: ProvidableConfiguration[] = [];

  /**
   * Configures IoC container
   * @param config
   * @param [externalDeps] @type ProvidableConfiguration[]
   */
  static configure(config: NetFlowCoreConfig, externalDependencies?: ProvidableConfiguration[]) {
    const { logging, ...coreConfig } = config;

    this.DEPENDENCIES = [...this.loadConfigDependencies(coreConfig), ...externalDependencies];

    Container.configure(...this.DEPENDENCIES);
    /**
     * Other dependencies to be resolved
     * Container.bind(...)....
     */

  }


  /**
   * Loads dependencies according to given config
   */
  private static loadConfigDependencies = (config: NetFlowCoreConfig) =>
    Object.keys(config)
      .map(key => BASE_DEPENDENCY_MAPPER[key](config[key]))
      .reduce((acc, val) => [...acc, ...val], []);
}
